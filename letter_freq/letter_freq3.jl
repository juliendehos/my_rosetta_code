h = zeros(Int, 26)
open(ARGS[1]) do filehandle
  for line in eachline(filehandle)
    for c in line
      x = lowercase(c)
      if x<='z' && x>='a'
        h[Int(x) + 1 - Int('a')] += 1
      end
    end
  end
end
println(div(1000*h, sum(h)))
