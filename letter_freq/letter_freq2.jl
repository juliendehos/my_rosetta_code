f = map(lowercase, filter(isalpha, readstring(ARGS[1])))
nb = length(f)
for c in 'a':'z'
    n = length(filter(x -> x == c, f))
    i = Int(c) - Int('a') + 1
    println(1000*n/nb)
end
