#include <iostream>
#include <fstream>
#include <numeric>
#include <vector>
using namespace std;
int main(int, char ** argv) {
    ifstream file(argv[1]);
    vector<int> freq(26, 0);
    string line;
    while (getline(file, line)) {
        for (char c : line) {
            if (c>='a' and c<='z') 
                freq[int(c-'a')]++;
            else if (c>='A' and c<='Z') 
                freq[int(c-'A')]++;
        }
    }
    int n = accumulate(freq.begin(), freq.end(), 0, plus<int>());
    for (int c : freq)
        cout << (c*1000)/n << endl;
    return 0;
}
