import System.Environment
import Data.Char
histo l = map toFreq instances
    where count xs x = length $ filter (==x) xs
          instances = map (count l) ['a'..'z']
          toFreq n = (1000 * n) `div` (length l)
main = getArgs >>= readFile.head >>= print.histo.(map toLower).(filter isAlpha)
