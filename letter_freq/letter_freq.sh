#!/bin/sh
awk -vFS="" '{ \
    for(i=1;i<=NF;i++) { \
        if($i~/[a-zA-Z]/) { \
            nb++;w[tolower($i)]++} } } \
    END{ \
    for(i in w) \
        print i,int(1000*w[i]/nb)}' $1 | sort
