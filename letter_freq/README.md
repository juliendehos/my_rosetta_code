# Letter frequencies

Compute letter frequencies in a text file

## Stats 

| Language     | time | nb lines | nb characters |
|--------------|------|----------|---------------|
| C++          | 0.02 | 22       | 571           |
| haskell      | 0.35 | 7        | 298           |
| julia        | 0.8  | 12       | 272           |
| lua (luajit) | 0.17 | 12       | 372           |
| python3      | 0.78 | 13       | 325           |
| sh           | 0.61 | 8        | 200           |


