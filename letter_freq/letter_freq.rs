use std::io;
use std::io::BufReader;
use std::io::BufRead;
use std::io::BufWriter;
use std::io::Write;
use std::fs::File;

fn main() {   
    let f = File::open("horace.txt").unwrap();
    let file = BufReader::new(&f);
    let mut writer = BufWriter::new(io::stdout());
    for (_num, line) in file.lines().enumerate() {
        let l = line.unwrap();
        let chars: String = l.chars().skip(1).collect(); 
        writeln!(writer, "{}", chars).unwrap();
    }           
}

