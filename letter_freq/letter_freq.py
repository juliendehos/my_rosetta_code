#!/usr/bin/env python3
import sys
import numpy as np
myFile = open(sys.argv[1], "r")
histo = np.zeros((26), dtype=np.int) 
for line in myFile:
    for c in line:
        k = ord(c.lower()) - ord('a')
        if (k>=0 and k<26):
            histo[k] = histo[k] + 1
n = sum(histo)
h = [int((1000*x)/n) for x in histo]
print(h)
