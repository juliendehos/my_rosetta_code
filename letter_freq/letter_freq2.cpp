#include <iostream>
#include <fstream>
#include <numeric>
#include <vector>
using namespace std;
int main(int, char ** argv) {
    ifstream file(argv[1]);
    vector<int> freq(26, 0);
    char c;
    while (file >> c) {
        if (c>='A' and c<='Z') 
            freq[int(c-'A')]++;
        if (c>='a' and c<='z') 
            freq[int(c-'a')]++;
    }
    int n = accumulate(freq.begin(), freq.end(), 0, plus<int>());
    for (int c : freq)
        cout << (c*1000)/n << endl;
    return 0;
}
