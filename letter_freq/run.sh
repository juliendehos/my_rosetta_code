#!/bin/sh

file="proust.txt"

delta_t () {
    ts0=$(echo $1 | cut -d, -f1)
    tn0=$(expr $(echo $1 | cut -d, -f2) \/ 1000000)
    ts1=$(echo $2 | cut -d, -f1)
    tn1=$(expr $(echo $2 | cut -d, -f2) \/ 1000000)
    dt=$(expr \( $ts1 - $ts0 \) \* 1000 + \( $tn1 - $tn0 \))
    return $dt
}

g++ -std=c++14 -O3 -o letter_freq_cpp.out letter_freq.cpp 
t0=$(date +"%s,%N")
./letter_freq_cpp.out $file > letter_freq_cpp.log
t1=$(date +"%s,%N")
delta_t $t0 $t1
dt=$?
stats=$(wc -lc letter_freq.cpp | awk '{print $1 "; " $2}')
echo "C++; $dt; $stats"

g++ -std=c++14 -O3 -o letter_freq2_cpp.out letter_freq2.cpp 
t0=$(date +"%s,%N")
./letter_freq2_cpp.out $file > letter_freq2_cpp.log
t1=$(date +"%s,%N")
delta_t $t0 $t1
dt=$?
stats=$(wc -lc letter_freq2.cpp | awk '{print $1 "; " $2}')
echo "C++; $dt; $stats"

t0=$(date +"%s,%N")
julia letter_freq.jl $file > letter_freq_jl.log
t1=$(date +"%s,%N")
delta_t $t0 $t1
dt=$?
stats=$(wc -lc letter_freq.jl | awk '{print $1 "; " $2}')
echo "julia; $dt; $stats"

luajit -b letter_freq.lua letter_freq_lua.out
t0=$(date +"%s,%N")
luajit letter_freq_lua.out $file > letter_freq_lua.log
t1=$(date +"%s,%N")
delta_t $t0 $t1
dt=$?
stats=$(wc -lc letter_freq.lua | awk '{print $1 "; " $2}')
echo "lua; $dt; $stats"

ghc -O2 -o letter_freq_hs.out letter_freq.hs
t0=$(date +"%s,%N")
./letter_freq_hs.out $file > letter_freq_hs.log
t1=$(date +"%s,%N")
delta_t $t0 $t1
dt=$?
stats=$(wc -lc letter_freq.hs | awk '{print $1 "; " $2}')
echo "haskell; $dt; $stats"

t0=$(date +"%s,%N")
./letter_freq.py $file > letter_freq_py.log
t1=$(date +"%s,%N")
delta_t $t0 $t1
dt=$?
stats=$(wc -lc letter_freq.py | awk '{print $1 "; " $2}')
echo "python3; $dt; $stats"

t0=$(date +"%s,%N")
./letter_freq.sh $file > letter_freq_sh.log
t1=$(date +"%s,%N")
delta_t $t0 $t1
dt=$?
stats=$(wc -lc letter_freq.sh | awk '{print $1 "; " $2}')
echo "sh; $dt; $stats"

