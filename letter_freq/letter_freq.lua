#!/usr/bin/env lua
data = {}
for i=1,26 do data[i] = 0 end
for line in io.lines(arg[1]) do 
    line:gsub(".", function(c)
        local l = string.byte(string.lower(c)) - string.byte('a') + 1
        if l>0 and l<=26 then data[l] = data[l] + 1 end
    end)
end
nb = 0
for k,v in pairs(data) do nb = nb + v end
for k,v in pairs(data) do print(k,math.floor(1000*v/nb)) end
