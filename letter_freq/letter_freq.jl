#!/usr/bin/env julia
function compute_hist(filename)
  h = zeros(Int, 26)
  for c in readstring(filename)
    x = lowercase(c)
    if x<='z' && x>='a'
      h[Int(x) + 1 - Int('a')] += 1
    end
  end
  h
end
h = compute_hist(ARGS[1])
println(1000*h / sum(h))
