#!/usr/bin/env python3
import math
import sys

def f(x):
    return math.sqrt(math.log1p(abs(x**1.5-x)))

def integrate_f(a, b, N):
    s = 0.0
    dx = (b-a)/N
    for i in range(N):
        s += f(a+i*dx)
    return s * dx

print(integrate_f(0.0, 10.0, int(sys.argv[1])))

