function integrate_f(f, a, b, N)
    dx = (b-a)/N
    s = @parallel (+) for i = 1:N
        f(a+i*dx) 
    end
    dx * s
end

@everywhere f(x) =  sqrt(log1p(abs(x^1.5-x)))

println(integrate_f(f, 0.0, 10.0, parse(Int, ARGS[1])))

