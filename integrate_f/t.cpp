#include<cmath>
#include<iostream>
#include<functional>

double integrate_f(std::function<double(double)> f, double a, double b, unsigned N) {
    double s = 0.0;
    const double dx = (b-a)/(double)N;
    for (unsigned i=0; i<N; i++)
        s += f(a+i*dx);
    return s * dx;
}

double f(double x) {
    return sqrt(log(1.0 + fabs(pow(x, 1.5)-x)));
}

int main(int argc, char ** argv) {
    std::cout << "t.cpp: " << integrate_f(f, 0.0, 10.0, atol(argv[1])) << std::endl;
    return 0;
}

