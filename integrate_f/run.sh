#!/usr/bin/env bash

gcc -o t_c.out -O2 t.c -lm
gcc -o t_smp_c.out -O2 -fopenmp t_smp.c -lm
g++ -o t_cpp.out -O2 t.cpp -lm
g++ -o t_smp_cpp.out -O2 -fopenmp t_smp.cpp -lm
julia t.jl 10 &> /dev/null
julia t_smp.jl 10 &> /dev/null
./setup.py build_ext --inplace
./t2.py 10 &> /dev/null
./t1.py 10 &> /dev/null

NEVALS=500000000

echo ""
echo "**** t.c ****"
time ./t_c.out $NEVALS

echo ""
echo "**** t_smp.c ****"
time ./t_smp_c.out $NEVALS

echo ""
echo "**** t.cpp ****"
time ./t_cpp.out $NEVALS

echo ""
echo "**** t_smp.cpp ****"
time ./t_smp_cpp.out $NEVALS

echo ""
echo "**** t.jl ****"
time julia t.jl $NEVALS

echo ""
echo "**** t_smp.jl ****"
time julia -p auto t_smp.jl $NEVALS

echo ""
echo "**** t2.py ****"
time ./t2.py $NEVALS

echo ""
echo "**** t1.py ****"
time ./t1.py $NEVALS

