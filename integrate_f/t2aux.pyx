import math

cdef double f(double x) except? -2:
    return math.sqrt(math.log1p(abs(x**1.5-x)))

cpdef double integrate_f(double a, double b, int N):
    cdef double s, dx
    cdef int i
    s = 0.0
    dx = (b-a)/N
    for i in range(N):
        s += f(a+i*dx)
    return s * dx

