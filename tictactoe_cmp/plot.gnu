set terminal png size 640,480
set style data linespoints
set grid xtics ytics
set logscale x
set xlabel 'nbSims'
set ylabel 'nbWins'
set key left top

set out 'out_cpp.png'
set title 'tictactoe (C++)'
plot 'out_cpp.dat' using 3:13 lw 2 title 'Monte-Carlo', 'out_cpp.dat' using 3:12 lw 2 title "Random", 'out_cpp.dat' using 3:11 lw 2 title 'Draws'

set out 'out_jl.png'
set title 'tictactoe (julia)'
plot 'out_jl.dat' using 3:13 lw 2 title 'Monte-Carlo', 'out_jl.dat' using 3:12 lw 2 title "Random", 'out_jl.dat' using 3:11 lw 2 title 'Draws'

