#include <algorithm>
#include <array>
#include <functional>
#include <iostream>
#include <random>

using namespace std;

const int NB_GAMES = 1000;

class Tictactoe {
  private:
    std::array<int,9> _board;
    int _nbFreeMoves;
    int _currentPlayer;
    int _nextPlayer;
    bool _finished;
    int _winner;

  public:
    Tictactoe() : 
      _board({0, 0, 0, 0, 0, 0, 0, 0, 0}),
      _nbFreeMoves(9),
      _currentPlayer(1),
      _nextPlayer(2),
      _finished(false),
      _winner(0) {
      }

    int getCurrentPlayer() const { return _currentPlayer; }

    int getNbFreeMoves() const { return _nbFreeMoves; }

    int getWinner() const { return _winner; }

    bool getFinished() const { return _finished; }

    bool playIndex(int moveIndex) {
      // find index
      int nbFree = moveIndex;
      int k = 0;
      while (k<9) {
        if (_board[k] == 0) {
          nbFree--;
          if (nbFree <= 0)
            break;
        }
        k++;
      }
      // update board
      if (k == 9) return false;
      _nbFreeMoves--;
      _board[k] = _currentPlayer;
      swap(_currentPlayer, _nextPlayer);
      // check winner
      auto & b = _board;
      _finished = true;
      _winner = 0;
      if (b[0] != 0 and b[0] == b[1] and b[1] == b[2])
        _winner = b[0];
      else if (b[3] != 0 and b[3] == b[4] and b[4] == b[5])
        _winner = b[3];
      else if (b[6] != 0 and b[6] == b[7] and b[7] == b[8])
        _winner = b[6];
      else if (b[0] != 0 and b[0] == b[3] and b[3] == b[6])
        _winner = b[0];
      else if (b[1] != 0 and b[1] == b[4] and b[4] == b[7])
        _winner = b[1];
      else if (b[2] != 0 and b[2] == b[5] and b[5] == b[8])
        _winner = b[2];
      else if (b[0] != 0 and b[0] == b[4] and b[4] == b[8])
        _winner = b[0];
      else if (b[2] != 0 and b[2] == b[4] and b[4] == b[6])
        _winner = b[2];
      else if (_nbFreeMoves != 0)
        _finished = false;
      return true;
    }
};

class Rng {
  private:
    std::mt19937_64 _engine;
    std::uniform_real_distribution<double> _distribution;

  public:
    Rng() : 
      _engine(random_device{}()),
      _distribution(0, 1) {
      }

    int operator()(int nMax) {
      return min(nMax-1, int(nMax*_distribution(_engine)));
    }
};

using genmove_t = function<int(const Tictactoe &)>;

genmove_t makeRandomBot() {
  Rng rng;
  genmove_t genmove = [rng] (const Tictactoe & game) mutable {
    int nbMoves = game.getNbFreeMoves();
    return rng(nbMoves);
  };
  return genmove;
}

void runGame(Tictactoe & game, genmove_t & bot1, genmove_t & bot2) {
  while (not game.getFinished()) {
    genmove_t & bot = game.getCurrentPlayer() == 1 ? bot1 : bot2;
    int moveIndex = bot(game);
    bool playOk = game.playIndex(moveIndex); 
    if (not playOk) {
      cerr << "runGame -> invalid play" << endl;
      exit(-1);
    }
  } 
}

genmove_t makeMcBot(int maxSims) {
  genmove_t randomBot = makeRandomBot();
  genmove_t genmove = [maxSims,randomBot] (const Tictactoe & game) mutable {
    int player = game.getCurrentPlayer();
    int nbMoves = game.getNbFreeMoves();
    int nbMoveSims = max(1, int(maxSims / nbMoves));
    int bestIndex = 0;
    int bestWins = 0;
    for (int moveIndex=0; moveIndex<nbMoves; moveIndex++) {
      int moveWins = 0;
      Tictactoe game1 = game;
      game1.playIndex(moveIndex);
      for (int sims=0; sims<nbMoveSims; sims++) {
        Tictactoe gameN = game1;
        runGame(gameN, randomBot, randomBot);
        if (gameN.getWinner() == player) 
          moveWins++;
      }
      if (moveWins > bestWins) {
        bestWins = moveWins;
        bestIndex = moveIndex;
      }
    }
    return bestIndex;
  };
  return genmove;
}

array<int,3> runGames(const Tictactoe & game, genmove_t & bot1, 
    genmove_t & bot2, int nbGames) {
  array<int,3> res = {0, 0, 0};
  for (int i=0; i<nbGames; i++) {
    Tictactoe testGame = game;
    runGame(testGame, bot1, bot2);
    int winner = testGame.getWinner();
    res[winner]++;
  }
  return res;
}

void runXp(int nbSims) {
  Tictactoe game;
  genmove_t botA = makeRandomBot();
  genmove_t botB = makeMcBot(nbSims);
  auto resAB = runGames(game, botA, botB, NB_GAMES);
  auto resBA = runGames(game, botB, botA, NB_GAMES);
  cout << "RandomBot McBot " << nbSims << " " << NB_GAMES << " "
    << resAB[0] << " " << resAB[1] << " " << resAB[2] << " " 
    << resBA[0] << " " << resBA[1] << " " << resBA[2] << " "
    << resAB[0]+resBA[0] << " " << resAB[1]+resBA[2] << " " << resAB[2]+resBA[1]
    << endl;
}

int main() {
  cout << "botA botB nbSims nbGames "
    << "drawsA1B2 winsA1 winsB2 drawsB1A2 winsB1 winsA2 draws winsA winsB\n";
  vector<int> vecNbSims {1, 10, 100, 1000, 10000};
  for_each(begin(vecNbSims), end(vecNbSims), runXp);
  return 0;
}

