# tictactoe_cmp

Some C++/Julia implementations of the tictactoe game with two bots (random and
Monte-Carlo), for comparing computation time.

| file                          | time |
|-------------------------------|------|
| tictactoe_cmp.cpp             | 12s  |
| tictactoe_cmp_lambda.cpp      | 12s  |
| tictactoe_cmp_lambda.jl       | 26s  |
| tictactoe_cmp_mutable.jl      | 26s  |
| tictactoe_cmp_mvector.jl      | 20s  |
| tictactoe_cmp_svector.jl      | 13s  |
| tictactoe_cmp_vector.jl       | 26s  |



## compare time

```
make
time ./tictactoe_cmp.out
time julia tictactoe_cmp_svector.jl
...
```

## compare bots

```
./tictactoe_cmp.out > out_cpp.dat
julia tictactoe_cmp_svector.jl > out_jl.dat
gnuplot plot.gnu
xdg-open out_jl.png
```

![](doc_plot.png)

