const NB_boardS = 1000

using StaticArrays
const EYE9 = eye(SMatrix{9, 9, Int})
const BOARD_LUT = SVector(EYE9, 2*EYE9)

const vec9 = SVector{9,Int}
const vec3 = SVector{3,Int}

@inline function computeFinish(b::SVector{9,Int}, nbFreeMoves::Int)
  if b[1] != 0 && b[1] == b[2] == b[3]
    true, b[1]
  elseif b[4] != 0 && b[4] == b[5] == b[6]
    true, b[4]
  elseif b[7] != 0 && b[7] == b[8] == b[9]
    true, b[7]
  elseif b[1] != 0 && b[1] == b[4] == b[7]
    true, b[1]
  elseif b[2] != 0 && b[2] == b[5] == b[8]
    true, b[2]
  elseif b[3] != 0 && b[3] == b[6] == b[9]
    true, b[3]
  elseif b[1] != 0 && b[1] == b[5] == b[9]
    true, b[1]
  elseif b[3] != 0 && b[3] == b[5] == b[7]
    true, b[3]
  elseif nbFreeMoves == 0  # no winner, no empty cell -> draw
    true, 0
  else  # no winner, empty cell -> game not finished
    false, 0
  end
end

@inline function findIndex(board::SVector{9,Int}, moveIndex::Int)
  nbFree = 0
  k = 1
  while k<=9 
    if board[k] == 0
      nbFree += 1
      if nbFree == moveIndex
        break
      end
    end
    k += 1
  end
  k
end

@inline function playIndex(board, last3, moveIndex::Int)
  k = findIndex(board, moveIndex)
  if k <= 9
    board = board + BOARD_LUT[last3[2]][k,:]
    board, vec3(last3[1]-1,last3[3],last3[2]) 
  else
    board, last3
  end
end

abstract type Bot end

@inline function runGame(board, last3, bot1::Bot, bot2::Bot)
  while true
    finished, winner = computeFinish(board, last3[1])
    if finished
      return winner
    end
    moveIndex = last3[2] == 1 ? genmove(board, last3, bot1) : genmove(board, last3, bot2) 
    board, last3 = playIndex(board, last3, moveIndex)
  end
end

struct RandomBot <: Bot end

@inline genmove(board, last3, bot::RandomBot) = trunc(Int, 1+rand()*last3[1]) 

struct McBot <: Bot
  maxSims::Int
end

function evalMove(board, last3, moveIndex::Int, nbSims::Int)
  bot = RandomBot()
  player = last3[2]
  board1,vvv3 = playIndex(board, last3, moveIndex)
  evalMove1(unused) = runGame(board1, vvv3, bot, bot) == player ? 1 : 0
  mapreduce(evalMove1, (+), 0, 1:nbSims)
end

function genmove(board, last3, bot::McBot)
  nbMoves = last3[1]
  nbMoveSims = max(1, div(bot.maxSims, nbMoves))
  simulate(moveIndex) = evalMove(board, last3, moveIndex, nbMoveSims)
  wins = map(simulate, [moveIndex for moveIndex in 1:nbMoves])     # 
  indmax(wins)
end

function runGames(board, last3, bot1::Bot, bot2::Bot, nbboards::Int)
  res = [0, 0, 0]
  for _ in 1:nbboards
    winner = 1 + runGame(board, last3, bot1, bot2)                # 
    res[winner] += 1
  end
  res
end

function runXp(nbSims::Int)
  board = vec9(0,0,0,0,0,0,0,0,0)
  last3 = vec3(9,1,2)
  botA = RandomBot()
  botB = McBot(nbSims)
  resAB = runGames(board, last3, botA, botB, NB_boardS)
  resBA = runGames(board, last3, botB, botA, NB_boardS)
  println("RandomBot McBot ", nbSims, " ", NB_boardS, " ", 
          resAB[1], " ", resAB[2], " ", resAB[3], " ", 
          resBA[1], " ", resBA[2], " ", resBA[3], " ", 
          resAB[1]+resBA[1], " ", resAB[2]+resBA[3], " ", resAB[3]+resBA[2])
end

println("botA botB nbSims nbboards drawsA1B2 winsA1 winsB2 drawsB1A2 winsB1 winsA2 draws winsA winsB")
vecNbSims = [1, 10, 100, 1000, 10000]
@time map(runXp, vecNbSims)


