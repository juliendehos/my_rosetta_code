# include("tictactoe_cmp_vector.jl")
include("tictactoe_cmp_svector.jl")
# include("tictactoe_cmp_mvector.jl")
# include("tictactoe_cmp_mutable.jl")
# include("tictactoe_cmp_lambda.jl")

using ProfileView

function main_profile()
  game = Tictactoe()
  botA = RandomBot()
  botB = McBot(1000)
  @profile runGames(game, botA, botB, NB_GAMES)
  Profile.print()
  ProfileView.view()
end

function main_run_once()
  game = Tictactoe()
  botA = RandomBot()
  botB = McBot(1000)
  runGames(game, botA, botB, NB_GAMES)
end

function main_run_all()
  println("botA botB nbSims nbGames drawsA1B2 winsA1 winsB2 drawsB1A2 winsB1 winsA2 draws winsA winsB")
  vecNbSims = [1, 10, 100, 1000, 10000]
  map(runXp, vecNbSims)
end
