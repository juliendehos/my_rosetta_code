import Control.Concurrent
import Control.Monad
import Data.Array
import Data.List

type Cell = Bool
type Ix2 = (Int, Int)
type Tab = Array Ix2 Cell

data World = World
    { _time :: Int
    , _space :: Tab
    }

createWorld :: (Ix2,Ix2) -> [Ix2] -> World
createWorld s@((i0,j0),(i1,j1)) ixs = 
    let space0 = listArray s $ replicate ((1+i1-i0)*(1+j1-j0)) False
    in World 0 (space0 // zip ixs (repeat True))

printWorld :: World -> IO ()
printWorld (World time space) = 
    let ((i0,j0), (i1,j1)) = bounds space
    in do
        putStrLn $ "\ntime: " ++ show time
        forM_ [i0..i1] $ \i -> do
            forM_ [j0..j1] $ \j -> do
                let alive = space ! (i,j)
                putStr $ if alive then "#" else "·"
            putStrLn ""

stepWorld :: World -> World
stepWorld (World time0 space0) = 
    let time1 = time0 + 1
        fUp alive livingNb = livingNb == 3 || livingNb == 2 && alive
        ijas = [(ij, fUp alive (livingNeighboors space0 ij)) | (ij, alive) <- assocs space0]
        space1 = space0 // ijas
    in World time1 space1

livingNeighboors :: Tab -> Ix2 -> Int
livingNeighboors tab ij0 = 
    let sizes = bounds tab
        fAcc acc ij = if tab!ij then acc+1 else acc
    in foldl' fAcc 0 (neighboors sizes ij0)

neighboors :: (Ix2, Ix2) -> Ix2 -> [Ix2]
neighboors ((i0,j0),(i1,j1)) (i,j) =
    [ (i',j') | di<-[-1..1], dj<-[-1..1], di/=0 || dj/=0, 
                let i'=i+di, let j'=j+dj, i'>=i0, i'<=i1, j'>=j0, j'<=j1 ]

stepGame :: Int -> World -> Int -> IO World
stepGame delay world0 _ = do
    let world1 = stepWorld world0
    threadDelay delay
    printWorld world1
    return world1

{-
main :: IO ()
main = do
    let world = createWorld ((-20,-60),(20,60)) [(0,0),(-1,0),(1,0),(0,1),(1,-1)]
    printWorld world
    foldM_ (stepGame 200000) world [1..200]
-}

main :: IO ()
main = do
    -- let world = createWorld ((-10,-20),(10,20)) [(0,-1),(0,0),(0,1),(0,2),(-1,0)]
    -- let world = createWorld ((-10,-20),(10,20)) [(0,0),(0,-1),(0,-2),(-1,0),(-2,-1)]
    let world = createWorld ((-10,-20),(10,20))
            [(0,0),(-1,0),(1,0),(0,-2),(0,2),(-1,-1),(-1,0),(-1,1),(1,-1),(1,0),(1,1)]
    printWorld world
    foldM_ (stepGame 500000) world [1..20]

{-
livingNeighboors :: Tab -> Ix2 -> Int
livingNeighboors tab ij0 = 
    let sizes@((i0,j0),(i1,j1)) = bounds tab
        fAcc acc ij@(i,j) | i<i0 || i>i1 || j<j0 || j>j1 = acc
                          | tab!ij = acc+1
                          | otherwise = acc
    in foldl' fAcc 0 (neighboors ij0)

neighboors :: Ix2 -> [Ix2]
neighboors (i,j) =
    [ (i+di,j+dj) | di<-[-1..1], dj<-[-1..1], di/=0 || dj/=0 ]
-}

