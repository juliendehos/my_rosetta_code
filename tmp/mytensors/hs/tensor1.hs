#!/usr/bin/env nix-shell
#!nix-shell -i runghc -p "haskellPackages.ghcWithPackages (pkgs: [pkgs.vector])

import qualified Data.Vector as V

data Tensor = Tensor
    { _vector :: V.Vector Double
    , _sizes :: [Int]
    } deriving (Eq, Show)

fill :: Double -> [Int] -> Tensor
fill x ss = Tensor vec ss
    where vec = V.fromList $ replicate n x
          n = product ss

ind :: [Int] -> [Int] -> Int
ind is ss = foldl f 0 iss
    where f acc (i, s) = acc*s + i
          iss = zip is ss

get :: Tensor -> [Int] -> Double
get (Tensor v ss) is = v V.! ind is ss

set :: Tensor -> [Int] -> Double -> Tensor
set arr@(Tensor v ss) is x = arr { _vector = v V.// [(ind is ss, x)] }

add :: Tensor -> Tensor -> Tensor
add (Tensor v1 s1) (Tensor v2 _) = Tensor (V.zipWith (+) v1 v2) s1

main :: IO ()
main = do
    let m0 = fill 0 [3, 2]
    let m1 = set m0 [1, 0] 42
    print m0
    print m1
    print $ get m0 [2, 1]
    print $ get m1 [2, 1]
    print $ add m0 m1

-- TODO error handling

