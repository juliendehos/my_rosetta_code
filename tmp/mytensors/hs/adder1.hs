#!/usr/bin/env nix-shell
#!nix-shell -i "runghc -Wall" -p "haskellPackages.ghcWithPackages (pkgs: [])

a1 :: [Int]
a1 = [1]

a3 :: [Int]
a3 = [1, 2, 3]

mysum :: [Int] -> Int
mysum = sum

main :: IO ()
main = do
    print $ mysum a1
    print $ mysum a3

