#!/usr/bin/env nix-shell
#!nix-shell -i "runghc -Wall" -p "haskellPackages.ghcWithPackages (pkgs: [])

{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE FlexibleInstances #-}

data Z = Z
    deriving Show

data (:.) a b = (:.) a b
    deriving Show

infixr 3 :.

type DIM0 = Z
type DIM1 = Int :. DIM0 

class Shape sh where
    mysum :: sh -> Int
    myadd :: sh -> sh -> sh

instance Shape Z where
    mysum Z = 0
    myadd Z Z = Z

instance Shape sh => Shape (Int :. sh) where
    mysum (n1 :. sh1) = n1 + mysum sh1
    myadd (n1 :. sh1) (n2 :. sh2) = n1 + n2 :. myadd sh1 sh2

a1 :: DIM1
a1 = 1 :. Z

a3 :: Int :. Int :. Int :. Z
a3 = 3 :. 2 :. 1 :. Z

main :: IO ()
main = do
    print $ mysum a1
    print $ mysum a3
    print $ myadd a3 (1 :. 1 :. 1:. Z)

-- http://hackage.haskell.org/package/repa-3.4.1.4/docs/doc-index-All.html
-- https://github.com/haskell-repa/repa/blob/master/repa/Data/Array/Repa/Index.hs
-- https://wiki.haskell.org/Numeric_Haskell:_A_Repa_Tutorial

