data StringKind = Block | Inline

type Block = 'Block
type Inline = 'Inline

data Substitution (a :: [StringKind]) where
  Content :: String -> Substitution '[ Block ]
  Txt :: String -> Substitution '[ Inline]

type family Sem (r :: Type) (a :: [ StringKind ]) where
  Sem r '[ Block ] = Substitution  '[ Block ]
  Sem r '[ Inline ] = Substitution '[ Inline]


newtype Plugin (r :: Type) (a :: [ StringKind ])
  = Plugin { substitute :: Substitution a -> Sem r a }


example :: String -> Substitution a -> Plugin r a
example = error "do me"

