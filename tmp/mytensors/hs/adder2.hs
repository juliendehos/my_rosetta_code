#!/usr/bin/env nix-shell
#!nix-shell -i "runghc -Wall" -p "haskellPackages.ghcWithPackages (pkgs: [])

{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE FlexibleInstances #-}

data Z = Z
    deriving Show

data (:.) a b = (:.) a b
    deriving Show

type DIM0 = Z
type DIM1 = DIM0 :. Int

class Shape sh where
    mysum :: sh -> Int
    myadd :: sh -> sh -> sh

instance Shape Z where
    mysum Z = 0
    myadd Z Z = Z

instance Shape sh => Shape (sh :. Int) where
    mysum (sh1 :. n1) = n1 + mysum sh1
    myadd (sh1 :. n1) (sh2 :. n2) = myadd sh1 sh2 :. (n1 + n2)

a1 :: DIM1
a1 = Z :. 1

a3 :: ((Z :. Int) :. Int) :. Int
a3 = Z :. 1 :. 2 :. 3

main :: IO ()
main = do
    print $ mysum a1
    print $ mysum a3
    print $ myadd a3 (Z :. 1 :. 1 :. 1)

-- http://hackage.haskell.org/package/repa-3.4.1.4/docs/doc-index-All.html
-- https://github.com/haskell-repa/repa/blob/master/repa/Data/Array/Repa/Index.hs
-- https://wiki.haskell.org/Numeric_Haskell:_A_Repa_Tutorial

