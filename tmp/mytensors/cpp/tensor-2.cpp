#include <algorithm>
#include <array>
#include <cassert>

template <int ... SN>
struct Tensor {

    static const int _K = (1 * ... * SN);
    std::array<double, _K> _data;
    static const int _D = sizeof...(SN);
    static constexpr std::array<int, _D> _dims {SN...};

    static_assert(_D > 0);

    void fill(const double & v) {
        _data.fill(v);
    }

    int ind(const std::array<int, _D> & inds) const {
        assert(inds[0]<_dims[0]);
        int k = inds[0];
        for (int i=1; i<_D; i++) {
            assert(inds[i]<_dims[i]);
            k = k*_dims[i] + inds[i];
        }
        return k;
    }

    double & operator()(const std::array<int, _D> & inds) {
        return _data[ind(inds)];
    }

    const Tensor<SN...> & operator+=(const Tensor<SN...> & t) {
        std::transform(_data.begin(), _data.end(), t._data.begin(),
                _data.begin(), std::plus<double>());
        return *this;
    }
};

template <int ... SN>
const Tensor<SN...> operator*(double k, const Tensor<SN...> & t1) {
    Tensor<SN...> t2;
    for (int i=0; i<t1._K; i++)
        t2._data[i] = k * t1._data[i];
    return t2;
}

#include <iostream>

int main() {

    Tensor<2,3,4> t1;
    t1.fill(7.0);
    Tensor<2,3,4> t2;
    t2.fill(3.0);
    Tensor<2,3,4> t3 = 2.0 * t1;

    t1 += t2;
    std::cout << t1({1,2}) << std::endl;
    std::cout << t2({1,2,3}) << std::endl;
    std::cout << t3({1,2,3}) << std::endl;

    t1({1,2,3}) = 0.0;
    std::cout << t1({1,2,3}) << std::endl;

    Tensor<2> t4;
    t4.fill(22.0);
    Tensor<2> t5;
    t5.fill(20.0);
    t4 += t5; 
    Tensor<2> t6 = 2.0 * t4;
    std::cout << t6({1}) << std::endl;

    return 0;
}

