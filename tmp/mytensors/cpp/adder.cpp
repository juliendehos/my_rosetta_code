#include <numeric>
#include <functional>
#include <vector>

///////////////////////////////////////////////////////////////////////////////
// runtime adder
// using std::vector
///////////////////////////////////////////////////////////////////////////////

struct Adder1 {
    int _result;
    Adder1(const std::vector<int> & values) {
        _result = std::accumulate(values.begin(), values.end(), 0, std::plus<int>());
    }
};

///////////////////////////////////////////////////////////////////////////////
// compile-time recursive adder
// using variadic template
///////////////////////////////////////////////////////////////////////////////

template<int ... SN> struct Adder2;

template<int S> 
struct Adder2<S> {
    const static int _result = S;
};

template<int S, int ... SN> 
struct Adder2<S, SN ...> {
    const static int _result = S + Adder2<SN...>::_result;
};

///////////////////////////////////////////////////////////////////////////////
// compile-time folding adder
// using variadic template + fold expression
///////////////////////////////////////////////////////////////////////////////

template <int ... SN>
struct Adder3 {
    int _result = (0 + ... + SN);
};

///////////////////////////////////////////////////////////////////////////////
// main
///////////////////////////////////////////////////////////////////////////////

#include <iostream>

int main() {

    Adder1 a1_1({1});
    std::cout << a1_1._result << std::endl;
    Adder1 a1_3({1,2,3});
    std::cout << a1_3._result << std::endl;

    Adder2<1> a2_1;
    std::cout << a2_1._result << std::endl;
    Adder2<1,2,3> a2_3;
    std::cout << a2_3._result << std::endl;

    Adder3<1> a3_1;
    std::cout << a3_1._result << std::endl;
    Adder3<1,2,3> a3_3;
    std::cout << a3_3._result << std::endl;

    return 0;
}

