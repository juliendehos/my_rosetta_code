#include <iostream>

int mul2(int x) {
    std::cout << "int mul2(int x)..." << std::endl;
    return x*2;
}

double mul2(double x) {
    std::cout << "double mul2(double int x)..." << std::endl;
    return x*2.0;
}

int main() {

    int n1 = 21;
    int r1 = mul2(n1);
    std::cout << n1 << " " << r1 << std::endl;

    double n2 = 2.1;
    double r2 = mul2(n2);
    std::cout << n2 << " " << r2 << std::endl;

    return 0;
}

