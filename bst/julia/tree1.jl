struct BstNode
   data::Float64
   left::Nullable{BstNode}
   right::Nullable{BstNode}
end

const Bst = Nullable{BstNode}

const NullBst = Bst()

function insert(t::Bst, x::Float64)
    if isnull(t)
        Bst(BstNode(x, NullBst, NullBst))
    else
        const n = get(t)
        if x < n.data
            const m = insert(n.left, x)
            Bst(BstNode(n.data, m, n.right))
        else
            const m = insert(n.right, x)
            Bst(BstNode(n.data, n.left, m))
        end
    end
end

function search(t::Bst, x::Float64)
    if isnull(t)
        false
    else
        const n = get(t)
        if n.data == x
            true
        elseif x < n.data
            search(n.left, x)
        else
            search(n.right, x)
        end
    end
end

t = NullBst
println(search(t, 37.0))
t = insert(t, 12.0)
t = insert(t, 42.0)
t = insert(t, 3.0)
t = insert(t, 37.0)
println(search(t, 37.0))
println(search(t, 7.0))
println(t)

