struct BstNode
   data::Float64
   left::Nullable{BstNode}
   right::Nullable{BstNode}
   parent::Nullable{BstNode}
end

const Bst = Nullable{BstNode}

const NullBst = Bst()

function insert(t::Bst, x::Float64, p=NullBst)
    if isnull(t)
        Bst(BstNode(x, NullBst, NullBst, p))
    else
        const n = get(t)
        if x < n.data
            const m = insert(n.left, x, t)
            Bst(BstNode(n.data, m, n.right, n.parent))
        else
            const m = insert(n.right, x, t)
            Bst(BstNode(n.data, n.left, m, n.parent))
        end
    end
end

function search(t::Bst, x::Float64)
    if isnull(t)
        false
    else
        const n = get(t)
        if n.data == x
            true
        elseif x < n.data
            search(n.left, x)
        else
            search(n.right, x)
        end
    end
end

function removeSub(t::Bst, x::Float64)
    t
end

t = NullBst
println(search(t, 37.0))
t = insert(t, 12.0)
println(t)
t = insert(t, 42.0)
println(t)
t = insert(t, 3.0)
t = insert(t, 37.0)
println(search(t, 37.0))
println(search(t, 7.0))
#println(t)
t = removeSub(t, 42.0)
#println(t)

