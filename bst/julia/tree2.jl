mutable struct Node
   data::Float64
   left::Int64
   right::Int64
   parent::Int64
end

struct Bst
    nodes::Vector{Node}
end

function initBst()
    Bst(Vector{Node}())
end

function insert!(t::Bst, x::Float64)
    const l = length(t.nodes) + 1
    parent = 0
    if l > 1
        parent = 1
        node = t.nodes[1]
        while true
            if x < node.data 
                if node.left == 0
                    node.left = l
                    break
                else
                    parent = node.left
                    node = t.nodes[parent]
                end
            else
                if node.right == 0
                    node.right = l
                    break
                else
                    parent = node.right
                    node = t.nodes[parent]
                end
            end
        end
    end
    push!(t.nodes, Node(x, 0, 0, parent))
end

function search(t::Bst, x::Float64)
    if length(t.nodes) == 0
        false
    else
        i = 1
        while i != 0
            node = t.nodes[i]
            if x == node.data
                return true
            elseif x < node.data
                i = node.left
            else
                i = node.right
            end
        end
        false
    end
end

function removeSub!(t::Bst, x::Float64)
    if length(t.nodes) != 0
        i = 1
        while i != 0
            node = t.nodes[i]
            if x == node.data
                j = node.left
                child = t.nodes[j]
                child.parent = node.parent
                parent = t.nodes[node.parent]
                if parent.left == i
                    parent.left = j
                else
                    parent.right = j
                end
                node.data = 0
                return
            elseif x < node.data
                i = node.left
            else
                i = node.right
            end
        end
    end
end

t = initBst()
println(search(t, 37.0))
insert!(t, 12.0)
insert!(t, 42.0)
insert!(t, 3.0)
insert!(t, 37.0)
println(search(t, 37.0))
println(search(t, 7.0))
println(t)
removeSub!(t, 42.0)
println(t)


