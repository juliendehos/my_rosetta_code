#!/usr/bin/env python3
import sys
if (len(sys.argv)!=2):
    print("usage: ", sys.argv[0], " <csv file>")
else:
    myFile = open(sys.argv[1], "r")
    data = []
    nbI = 0
    for line in myFile:
        nbI = nbI + 1
        l = line.split()[0]
        row = [n for n in l.split(";")]
        data = data + row
    nbK = len(data)
    nbJ = int(nbK / nbI)
    delta1 = 1
    delta2 = nbJ
    nb1 = nbJ
    nb2 = nbI - 1
    p = -1
    k = 0
    while k < nbK:
        for _ in range(nb1):
            k = k + 1
            p = p + delta1
            print(data[p], end=" ")
        delta1 = delta1 * -1
        delta1, delta2 = delta2, delta1
        nb1 = nb1 - 1
        nb1, nb2 = nb2, nb1
    print()
