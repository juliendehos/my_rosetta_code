import System.Environment
import Data.List.Split
import Data.List
ops = cycle [(head,tail),(last,init),(reverse.last,init),(reverse.head,tail)]
spiral::[([[String]]->[String],[[String]]->[[String]])]-> [[String]]-> [String]
spiral _ [] = []
spiral ((oh,ot):os) mat = h ++ (spiral os (transpose t))
    where h = oh mat
          t = ot mat
parse = (map (splitOn ";")).lines 
main = getArgs >>= readFile.head >>= putStrLn.unwords.(spiral ops).parse
