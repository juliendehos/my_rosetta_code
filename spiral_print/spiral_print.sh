#!/bin/sh
if [ $# -ne 1 ] ; then
    echo "usage: $0 <csv file>"
else
    tr ';' ' ' < $1 > tmp1 # preprocessing
    echo "" > res
    while [ -s tmp1 ] ; do
        head -n 1 tmp1 >> res # extract first line
        tail -n +2 tmp1 > tmp2
        awk '{print $NF}' tmp2 >> res # extract last column
        awk 'NF{NF--};1' tmp2 > tmp3
        tail -n 1 tmp3 | tr ' ' '\n' | tac >> res # extract last line
        head -n -1 tmp3 > tmp4
        cut -d" "  -f1 < tmp4 | tac >> res # extract first column
        cut -d" "  -f2- < tmp4 > tmp1
    done
    tr '\n' ' ' < res # postprocessing
    rm -f tmp1 tmp2 tmp3 tmp4 res
fi
