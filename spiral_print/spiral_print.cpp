#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>
using namespace std;
int main(int argc, char ** argv) {
    if (argc != 2) {
        cout << "usage: " << argv[0] << " <csv file>" << endl;
        exit(-1);
    }
    fstream fileStream(argv[1]);
    string line;
    int nbI = 0;
    vector<string> matrix;
    while (getline(fileStream, line)) {
        nbI++;
        stringstream lineStream(line);
        string value;
        while (getline(lineStream, value, ';')) {
            matrix.push_back(value);
        }
    }
    int nbK = matrix.size();
    int nbJ = nbK / nbI;
    int delta1 = 1;        // don't even try to write an understandable comment
    int delta2 = nbJ;
    int nb1 = nbJ;
    int nb2 = nbI - 1;
    int p = -1;
    int k=0;
    while (k<nbK) {
        for (int n=0; n<nb1; n++) {
            k++;
            p += delta1;
            cout << matrix[p] << " ";
        }
        delta1 *= -1;
        swap(delta1, delta2);
        nb1--;
        swap(nb1, nb2);
    }
    cout << endl;
    return 0;
}
