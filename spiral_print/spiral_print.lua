#!/usr/bin/env lua
if #arg ~= 1 then  -- check arguments
    print("usage: ", arg[0], " <csv file>")
    exit(-1)
end
data = {}  -- read csv in a table
nbI = 0
for line in io.lines(arg[1]) do 
    nbI = nbI + 1
    for token in string.gmatch(line, "[^;]+") do
        table.insert(data, token)
    end
end
nbJ = #data / nbI  -- spiral printing
delta1 = 1
delta2 = nbJ
nb1 = nbJ
nb2 = nbI - 1
p = 0
k = 0
while k < #data do
    for n = 1,nb1 do
        k = k + 1
        p = p + delta1
        io.write(data[p], " ")
    end
    delta1 = delta1 * -1
    delta1, delta2 = delta2, delta1
    nb1 = nb1 - 1
    nb1, nb2 = nb2, nb1
end
print()
