#!/usr/bin/env perl
die "usage $0 <csv file>" if $#ARGV != 0;
open(my $fh, '<', $ARGV[0]);
my @data;
my $nbI = 0;
while (my $line = <$fh>) {
    chomp($line);
    my @tokens = split(';', $line);
    push(@data, @tokens);
    $nbI++
}
my $nbK = 1 + $#data;
my $nbJ = $nbK / $nbI;
my $delta1 = 1;
my $delta2 = $nbJ;
my $nb1 = $nbJ;
my $nb2 = $nbI - 1;
my $p = -1;
my $k = 0;
while ($k < $nbK) {
    for (1..$nb1) { 
        $k = $k + 1;
        $p = $p + $delta1;
        print(@data[$p], " ");
    }
    $delta1 = $delta1 * -1;
    ($delta1, $delta2) = ($delta2, $delta1);
    $nb1 = $nb1 - 1;
    ($nb1, $nb2) = ($nb2, $nb1);
}
print("\n");
