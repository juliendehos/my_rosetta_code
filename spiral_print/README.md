# Spiral print

print a matrix in spiral order (starting from top-left, clockwise)

## input

the name of a csv file 

```
$ cat input1.csv 
1;2;3;4
5;6;7;8
9;10;11;12
```

## output

spiral print the matrix in the standard output

```
1 2 3 6 9 8 7 4 5
```

## code stats 

Language | nb lines | nb characters
---|---|---|
C | 48 | 1214
C++ | 44 | 1057
Haskell (functional)| 11 | 448
Lua | 32 | 639
Perl | 31 | 641
Python | 31 | 708
Sh | 19 | 627

## TODO
python code in functional style

