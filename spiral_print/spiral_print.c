// gcc -std=gnu99 -o spiral_print_c.out spiral_print.c 
// ./spiral_print_c.out input1.csv
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
int main(int argc, char *argv[])
{
    if (argc != 2) {
        fprintf(stderr, "usage: %s <csv file>\n", argv[0]);
        exit(-1);
    }
    FILE * fh = fopen(argv[1] , "r");
    char buffer[10000];
    char data[10000][10];
    int nbI = 0;
    int nbK = 0;
    while (1 == fscanf(fh, "%[^\n]%*c", buffer)) { 
        nbI++;
        char * ptrBuffer;
        char * saveptr;
        for (ptrBuffer = buffer; ; nbK++, ptrBuffer = NULL) {
            char * token = strtok_r(ptrBuffer, ";", &saveptr);
            if (token == NULL) break;
            memcpy(data[nbK], token, strlen(token));
        }
    }
    int nbJ = nbK / nbI;
    int delta1 = 1;
    int delta2 = nbJ;
    int nb1 = nbJ;
    int nb2 = nbI - 1;
    int p = -1;
    int k=0;
    while (k<nbK) {
        for (int n=0; n<nb1; n++) {
            k++;
            p += delta1;
            printf("%s ", data[p]);
        }
        delta1 *= -1;
        delta2 = delta1 + delta2 - (delta1 = delta2);
        nb1--;
        nb2 = nb1 + nb2 - (nb1 = nb2);
    }
    printf("\n");
    return 0;
}

