// from vitalyd
// https://users.rust-lang.org/t/functional-programming-in-rust-code-review/17628/5

use std::env;
use std::fmt;
use std::fs::File;
use std::io;
use std::io::prelude::*;

#[derive(Debug)]
struct Stats {
    avg: f64,
    var: f64,
    algo: String,
    params: Vec<f64>,
}

fn compute_stats(data: &[f64], algo: &str, params: Vec<f64>) -> Stats {
    let n = data.len() as f64;
    let avg = data.iter().sum::<f64>() / n;
    let var = data.iter().fold(0.0, |acc, x| acc + (x - avg).powf(2.0)) / n;
    Stats {
        avg,
        var,
        algo: algo.into(),
        params,
    }
}

impl fmt::Display for Stats {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        writeln!(f, "average; variance; algo; params")?;
        write!(
            f,
            "{}; {}; {}; {}",
            self.avg,
            self.var,
            self.algo,
            self.params
                .iter()
                .map(ToString::to_string)
                .collect::<Vec<String>>()
                .join("|")
        )
    }
}

fn read<R: Read>(mut io: R) -> io::Result<Vec<f64>> {
    let contents = &mut String::new();
    io.read_to_string(contents)?;
    contents
        .split_whitespace()
        .map(|x| {
            x.parse()
                .map_err(|e| io::Error::new(io::ErrorKind::InvalidData, e))
        })
        .collect()
}

fn create_func(algo: &str, params: &[f64]) -> Result<Box<Fn(&f64) -> f64>, String> {
    Ok(match (algo, params.len()) {
        ("mul2", 0) => Box::new(|x| x * 2.0),
        ("mul", 1) => {
            let k = params[0];
            Box::new(move |x| x * k)
        }
        ("sin", 2) => {
            let a = params[0];
            let b = params[1];
            Box::new(move |x| (x * a + b).sin())
        }
        _ => Err(format!(
            "unrecognized algo or invalid args: {} {:?}",
            algo, params
        ))?,
    })
}

fn main() -> Result<(), Box<std::error::Error>> {
    let args: Vec<String> = env::args().collect();
    if args.len() < 3 {
        return Err(format!("usage: {} <input> <algo> <params>", args[0]).into());
    }
    let filename = &args[1];
    let algo = &args[2];

    let params = (&args[3..])
        .iter()
        .map(|x| x.parse())
        .collect::<Result<Vec<f64>, _>>()?;

    let func = create_func(algo, &params)?;
    let data = read(File::open(filename)?)?;
    let data = data.iter().map(|x| func(x)).collect::<Vec<_>>();
    let stats = compute_stats(&data, algo, params);

    println!("{}", stats);
    Ok(())
}
