#!/usr/bin/env julia

struct Stats
  avg::Float64
  var::Float64
  algo::String
  params::Vector{Float64}
end

# compute statistics on data 
function computeStats(data::Vector{Float64}, algo, params)
  const myAvg = mean(data)
  const myVar = var(data, corrected=false)
  Stats(myAvg, myVar, algo, params)
end

# print statistics
function printStats(stats::Stats)
  const pStr = join(map(string, stats.params), "|")
  @printf("%f; %f; %s; %s\n", stats.avg, stats.var, stats.algo, pStr)
end

# read numbers from a file
readParseFile = vec ∘ readdlm

# build and return a function, or an error message
function createFunc(algo::String, prms::Vector{Float64})
  if (algo == "mul2")
    length(prms) != 0 ? "args for x*2: mul2" : x -> x*2
  elseif (algo == "mul")
    if length(prms) != 1 
      "args for x*k: mul <k>" 
    else
      k = prms[1]
      x -> x*k
    end
  elseif (algo == "sin")
    if length(prms) != 2 
      "args for sin(a*x+b): sin <a> <b>" 
    else
      a, b = prms
      x -> sin(x*a + b)
    end
  else
    "unknown algo"
  end
end

# main program
if (!isinteractive())
  if (length(ARGS) < 2)
    const prog = basename(Base.source_path())
    println("usage: ./", prog, " <input> <algo> <params>")
    exit(-1)
  end
  const filename, algo = ARGS
  const params = map(x -> parse(Float64, x), ARGS[3:end])

  strorfunc = createFunc(algo, params)
  if (typeof(strorfunc) == String)
    println(strorfunc)
    exit(-1)
  else
    const data1 = readParseFile(filename)
    const data2 = map(strorfunc, data1)
    const stats = computeStats(data2, algo, params)
    println("average; variance; algo; params")
    printStats(stats)
  end
end

