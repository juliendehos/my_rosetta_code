// from vitalyd
// https://users.rust-lang.org/t/functional-programming-in-rust-code-review/17628/8

use std::env;
use std::fmt;
use std::fs::File;
use std::io;
use std::io::prelude::*;

#[derive(Debug)]
struct Stats {
    avg: f64,
    var: f64,
    algo: String,
    params: Vec<f64>,
}

#[derive(Debug)]
enum Algo {
    Mul2,
    Mul(f64),
    Sin { a: f64, b: f64 },
}

impl fmt::Display for Algo {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Algo::Mul2 => f.write_str("x * 2.0"),
            Algo::Mul(by) => write!(f, "x * {}", by),
            Algo::Sin { a, b } => write!(f, "sin(x * {} + {})", a, b),
        }
    }
}

impl Algo {
    fn from_args(name: &str, params: &[f64]) -> Result<Self, String> {
        Ok(match (name, params.len()) {
            ("mul2", 0) => Algo::Mul2,
            ("mul", 1) => Algo::Mul(params[0]),
            ("sin", 2) => Algo::Sin {
                a: params[0],
                b: params[1],
            },
            _ => Err(format!(
                    "unrecognized algo or invalid args: {} {:?}",
                    name, params
                    ))?,
        })
    }

    fn apply(&self, input: f64) -> f64 {
        match *self {
            Algo::Mul2 => input * 2.0,
            Algo::Mul(by) => input * by,
            Algo::Sin { a, b } => (input * a + b).sin(),
        }
    }
}

fn compute_stats(data: &[f64], algo: &str, params: Vec<f64>) -> Stats {
    let n = data.len() as f64;
    let avg = data.iter().sum::<f64>() / n;
    let var = data.iter().fold(0.0, |acc, x| acc + (x - avg).powf(2.0)) / n;
    Stats {
        avg,
        var,
        algo: algo.into(),
        params,
    }
}

impl fmt::Display for Stats {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        writeln!(f, "average; variance; algo; params")?;
        write!(
            f,
            "{}; {}; {}; {}",
            self.avg,
            self.var,
            self.algo,
            self.params
            .iter()
            .map(ToString::to_string)
            .collect::<Vec<String>>()
            .join("|")
            )
    }
}

fn read<R: Read>(mut io: R) -> io::Result<Vec<f64>> {
    let contents = &mut String::new();
    io.read_to_string(contents)?;
    contents
        .split_whitespace()
        .map(|x| {
            x.parse()
                .map_err(|e| io::Error::new(io::ErrorKind::InvalidData, e))
        })
    .collect()
}

fn main() -> Result<(), Box<std::error::Error>> {
    let args: Vec<String> = env::args().collect();
    if args.len() < 3 {
        return Err(format!("usage: {} <input> <algo> <params>", args[0]).into());
    }
    let filename = &args[1];
    let algo_str = &args[2];

    let params = (&args[3..])
        .iter()
        .map(|x| x.parse())
        .collect::<Result<Vec<f64>, _>>()?;

    let algo = Algo::from_args(algo_str, &params)?;
    let data = read(File::open(filename)?)?;
    let data = data.iter().map(|&x| algo.apply(x)).collect::<Vec<_>>();
    let stats = compute_stats(&data, algo_str, params);

    println!("{}", stats);
    Ok(())
}

