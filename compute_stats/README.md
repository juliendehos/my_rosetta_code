# compute_stats

a simple program for testing the "functional programming style":

- read numbers from a file
- build a function from command line arguments
- map this function over these numbers
- compute some statistics on the results 


## Haskell

ghc 8.2.2

```
$ runghc compute_stats.hs input.txt mul2
average; variance; algo; params
3.7500; 2.4375; mul2; 

$ runghc compute_stats.hs input.txt mul 42
average; variance; algo; params
78.7500; 1074.9375; mul; 42.0

$ runghc compute_stats.hs input.txt sin 0.5 2
average; variance; algo; params
0.1897; 0.1366; sin; 0.5|2.0
```

## C++

gcc 7.3.0

```
$ g++ -std=c++17 -o compute_stats compute_stats.cpp 

$ ./compute_stats input.txt mul2
average; variance; algo; params
3.75; 2.4375; mul2; 

...
```


## Julia

julia 0.6.2

```
$ ./compute_stats.jl input.txt mul2
average; variance; algo; params
3.750000; 2.437500; mul2; 

...
```


## Rust

rustc 1.26.0

```
$ rustc +stable compute_stats_3.rs

$ ./compute_stats_3 input.txt mul2
average; variance; algo; params
3.75; 2.4375; mul2; 

...
```

