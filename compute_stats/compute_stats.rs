use std::env;
use std::fs::File;
use std::io::prelude::*;
use std::process;

struct Stats {
    avg: f64,
    var: f64,
    algo: String,
    params: Vec<f64>
}

// compute statistics on data 
// get ownership for data and params
fn compute_stats(data: Vec<f64>, algo: &str, params: Vec<f64>) -> Stats {
    let n = data.len() as f64;
    let avg = data.iter().sum::<f64>() / n;
    let var = data.iter().fold(0.0, |acc, x| acc + (x-avg).powf(2.0)) / n;
    let algo = algo.to_string();
    Stats {avg, var, algo, params}
}

// print statistics
// use borrowing for stats
fn print_stats(stats: &Stats) {
    let p_strs: Vec<String> 
        = stats.params.iter().map(|x| x.to_string()).collect();
    let p_str = p_strs.join("|");
    println!("{}; {}; {}; {}", stats.avg, stats.var, stats.algo, p_str);
}

// read numbers from a file
fn read_parse_file(filename: &str) -> Vec<f64> {
    let mut file = File::open(filename).expect("file not found");
    let mut contents = String::new();
    file.read_to_string(&mut contents).unwrap();
    contents.split_whitespace()
            .map(|x| x.parse::<f64>().unwrap())
            .collect()
}

// build and return a function, or an error message
fn create_func(algo: &str, params: &[f64]) -> 
Result<Box<Fn(&f64) -> f64>, String> {
    let n = params.len();
    match algo {
        "mul2" => {
            if n != 0 { return Err("args for x*2: mul2".to_string()); }
            return Ok(Box::new(move |x| x * 2.0)); 
        }
        "mul" => {
            if n != 1 { return Err("args for x*k: mul <k>".to_string()); }
            let k = params[0];
            return Ok(Box::new(move |x| x * k));
        }
        "sin" => {
            if n != 2 { 
                return Err("args for sin(a*x+b): sin <a> <b>".to_string()); 
            }
            let a = params[0];
            let b = params[1];
            return Ok(Box::new(move |x| (x*a + b).sin()));
        }
        _ => Err("unknown algo".to_string())
    }
}

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() < 3 {
        println!("usage: {} <input> <algo> <params>", args[0]);
        process::exit(-1);
    }
    let filename = &args[1];
    let algo = &args[2];

    let params: Vec<f64> = 
        (&args[3..]).iter().map(|x| x.parse::<f64>().unwrap()).collect();

    let func = match create_func(algo, &params) {
        Ok(f) => f,
        Err(s) => { 
            println!("{}", s); 
            process::exit(-1); 
        }
    };

    let data1 = read_parse_file(filename);
    let data2 = data1.iter().map(|x| func(x)).collect();
    let stats = compute_stats(data2, algo, params);

    println!("average; variance; algo; params");
    print_stats(&stats);
}

