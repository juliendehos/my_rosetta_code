#!/usr/bin/env python3
import random
import sys
if (len(sys.argv)!=2):
    print("usage: ", sys.argv[0], " <nb samples>")
else:
    nbSamples = int(sys.argv[1])
    def rand2(): x = random.uniform(0.0, 1.0); return x*x
    nbDisk = sum ([1 for i in range(nbSamples) if rand2() + rand2() < 1])
    print("myPi = ", 4.0 * nbDisk / float(nbSamples))
