-- ghc --make -O2 mc_pi.hs
-- time ./mc_pi 10000000
import System.Environment   
import System.Random
sum2 acc [] = acc
sum2 acc (x:y:xs) = (if x*x + y*y < 1 then (sum2 (acc+1) xs) else (sum2 acc xs))
mcPi :: RandomGen a => Int -> a -> Double
mcPi nbSamples g = 4.0 * (fromIntegral nbDisk) / (fromIntegral nbSamples)
    where nbDisk = sum2 0 $ take (2*nbSamples) (randoms g :: [Double])
main = do args@(n:_) <- getArgs
          name <- getProgName
          g <- getStdGen
          if length args /= 1 then print $ "usage: "++name++" <nbSamples>" 
              else print $ mcPi (read n::Int) g
