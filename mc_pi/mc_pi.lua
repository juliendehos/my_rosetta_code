-- time luajit mc_pi.lua 10000000
if #arg ~= 1 then
    print("usage: ", arg[0], " <nb samples>")
else
    local nbSamples = arg[1]
    local nbDisk = 0
    for i = 1, nbSamples do
        x = math.random()
        y = math.random()
        if x*x + y*y < 1 then
            nbDisk = nbDisk + 1
        end
    end
    print("myPi = ", 4.0 * nbDisk / nbSamples)
end
