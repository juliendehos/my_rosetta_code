#!/usr/bin/env perl
die "usage $0 <nb samples>" if $#ARGV != 0;
sub rand2{ my $x = rand; return $x*$x }
my $nbSamples = $ARGV[0];
my $nbDisk = 0;
for (1..$nbSamples) { $nbDisk += rand2() + rand2() < 1 ? 1 : 0 }
print "myPi = ", (4 * $nbDisk / $nbSamples), "\n";
