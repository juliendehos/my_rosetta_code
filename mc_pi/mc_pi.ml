(* ocamlopt mc_pi.ml && time ./a.out 10000000 *)
let rand2 () = let x = Random.float 1. in x *. x 
let () =
    let n = int_of_string Sys.argv.(1) in
    let p = ref 0 in
    for k = 1 to n do
        if rand2() +. rand2() <= 1. then p := !p + 1
    done;
  let myPi = 4. *. float !p /. float n in
  Printf.printf "myPi = %f\n" myPi

