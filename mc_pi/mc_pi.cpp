// g++ -std=c++11 -O3 -o mc_pi_cpp.out mc_pi.cpp 
// time ./mc_pi_cpp.out 10000000
#include <iostream>
#include <random>
using namespace std;
int main(int argc, char ** argv) {
    if (argc != 2) {
        cout << "usage: " << argv[0] << " <nb samples>" << endl;
        exit(-1);
    }
    const int nbSamples = stoi(argv[1]);
    mt19937 engine(random_device{}());
    uniform_real_distribution<float> distribution(0, 1);
    int nbDisk = 0;
    for (int i=0; i<nbSamples; i++) {
        double x = distribution(engine); 
        double y = distribution(engine); 
        if (x*x + y*y < 1)
            nbDisk++;
    }
    cout << "myPi = " << 4.0 * nbDisk / double(nbSamples) << endl;
    return 0;
}
