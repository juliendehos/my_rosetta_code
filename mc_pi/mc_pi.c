// gcc -O3 -std=gnu99 -o mc_pi_c.out mc_pi.c 
// time ./mc_pi_c.out 10000000
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
int main(int argc, char ** argv) {
    if (argc != 2) {
        printf("usage: %s <nb samples>\n", argv[0]); 
        exit(-1);
    }
    const int nbSamples = atoi(argv[1]);
    srand48(time(0));
    int nbDisk = 0;
    for (int i=0; i<nbSamples; i++) {
        double x = drand48();
        double y = drand48();
        if (x*x + y*y < 1)
            nbDisk++;
    }
    printf("myPi = %f\n", 4.0 * nbDisk / (double)nbSamples);
    return 0;
}
