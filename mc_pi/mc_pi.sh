#!/bin/sh
if [ $# -ne 1 ] ; then
    echo "usage: $0 <nb samples>"
else
    echo | seq 1 $1 | awk -v r=0 -v n=$1 'BEGIN{srand()} \
        {x=rand(); y=rand(); if (x*x+y*y<1) {r++}} END{print 4*r/n}'
fi
