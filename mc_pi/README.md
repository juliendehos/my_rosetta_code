# Monte Carlo Pi

Compute pi using the Monte Carlo method


## Algorithm

- S = set of N random samples in [0,1]^2
- D = count samples S_i where ||S_i|| < 1
- return 4*D/N


## code stats (10 runs)

Language | result | time | nb lines | nb characters
---|---|---|---|---|
C | 3.1417| 0.199 | 22 | 581
C++ | 3.1415 | 0.178 | 23 | 704
Haskell | |  | 14 | 599
Lua | 3.1415 | 0.243 | 15 | 366
Ocaml | 3.1413 | 1.348 | 11 | 334
Perl 5 | 3.1417| 6.217 | 7 | 262
Python 3 | 3.1414| 8.214 | 10 | 347
Sh | 3.1416| 1.801 | 7 | 205


## TODO
- haskell version sucks

