// g++ -std=c++11 -Wall -Wextra `pkg-config --cflags opencv` -o mandelbrot.out mandelbrot.cpp `pkg-config --libs opencv` -fopenmp

#include <chrono>
#include <complex>
#include <functional>
#include <iostream>
#include <opencv2/opencv.hpp>

using namespace std;

const int HEIGHT = 2400;
const int WIDTH = 2700;
//const int HEIGHT = 4800;
//const int WIDTH = 5400;
const int MAXITER = 100;
//const int MAXITER = 500;
const float X1 = -2.1f;
const float X2 = 0.6f;
const float Y1 = -1.2f;
const float Y2 = 1.2f;
const float SCALEX = WIDTH / (X2 - X1);
const float SCALEY = HEIGHT / (Y2 - Y1);

class Chrono {
    private:
        chrono::time_point<chrono::system_clock> _t0;
        chrono::time_point<chrono::system_clock> _t1;
    public:
        void start() {
            _t1 = _t0 = chrono::system_clock::now();
        }
        void stop() {
            _t1 = chrono::system_clock::now();
        }
        double elapsed() const {
            auto d = chrono::duration_cast<chrono::milliseconds>(_t1 - _t0);
            return 1e-3 * d.count();
        }
};

using mandelbrot_t = function<void(cv::Mat &, const float, const float, 
        const float, const float, const int)>; 

void run_mandelbrot(const string & name, mandelbrot_t f) {
    Chrono chrono;
    cv::Mat img(HEIGHT, WIDTH, CV_8U);
    chrono.start();
    f(img, X1, Y1, SCALEX, SCALEY, MAXITER);
    chrono.stop();
    cout << name << ": " << chrono.elapsed() << endl;
    cv::imwrite(string("out_") + name + ".png", img);
}

int mandelbrot(const complex<float> &z0, const int max) {
    complex<float> z = z0;
    for (int t = 0; t < max; t++) {
        if (z.real()*z.real() + z.imag()*z.imag() > 4.0f) return t;
        z = z*z + z0;
    }
    return max;
}

int mandelbrotFormula(const complex<float> &z0, const int maxIter) {
    const int value = mandelbrot(z0, maxIter);
    if(maxIter - value == 0) return 0;
    return cvRound(sqrt(value / (float) maxIter) * 255);
}

void mandelbrot_at(cv::Mat &img, const float x1, const float y1, 
        const float scaleX, const float scaleY, const int maxIter) {
    for (int i = 0; i < img.rows; i++) {
        for (int j = 0; j < img.cols; j++) {
            const float x0 = j / scaleX + x1;
            const float y0 = i / scaleY + y1;
            const complex<float> z0(x0, y0);
            const uchar value = (uchar) mandelbrotFormula(z0, maxIter);
            img.at<uchar>(i, j) = value;
        }
    }
}

void mandelbrot_omp_at(cv::Mat &img, const float x1, const float y1, 
        const float scaleX, const float scaleY, const int maxIter) {
#pragma omp parallel for
    for (int i = 0; i < img.rows; i++) {
        for (int j = 0; j < img.cols; j++) {
            const float x0 = j / scaleX + x1;
            const float y0 = i / scaleY + y1;
            const complex<float> z0(x0, y0);
            const uchar value = (uchar) mandelbrotFormula(z0, maxIter);
            img.at<uchar>(i, j) = value;
        }
    }
}

void mandelbrot_ptr(cv::Mat &img, const float x1, const float y1, 
        const float scaleX, const float scaleY, const int maxIter) {
    for (int i = 0; i < img.rows; i++) {
        for (int j = 0; j < img.cols; j++) {
            const float x0 = j / scaleX + x1;
            const float y0 = i / scaleY + y1;
            const complex<float> z0(x0, y0);
            const uchar value = (uchar) mandelbrotFormula(z0, maxIter);
            img.ptr<uchar>(i)[j] = value;
        }
    }
}

void mandelbrot_omp_ptr(cv::Mat &img, const float x1, const float y1, 
        const float scaleX, const float scaleY, const int maxIter) {
#pragma omp parallel for
    for (int i = 0; i < img.rows; i++) {
        for (int j = 0; j < img.cols; j++) {
            const float x0 = j / scaleX + x1;
            const float y0 = i / scaleY + y1;
            const complex<float> z0(x0, y0);
            const uchar value = (uchar) mandelbrotFormula(z0, maxIter);
            img.ptr<uchar>(i)[j] = value;
        }
    }
}

void mandelbrot_data(cv::Mat &img, const float x1, const float y1, 
        const float scaleX, const float scaleY, const int maxIter) {
    for (int k = 0; k < img.rows*img.cols; k++) {
        const int i = k/img.cols;
        const int j = k%img.cols;
        const float x0 = j / scaleX + x1;
        const float y0 = i / scaleY + y1;
        const complex<float> z0(x0, y0);
        const uchar value = (uchar) mandelbrotFormula(z0, maxIter);
        img.data[k] = value;
    }
}

void mandelbrot_omp_data(cv::Mat &img, const float x1, const float y1, 
        const float scaleX, const float scaleY, const int maxIter) {
#pragma omp parallel for
    for (int k = 0; k < img.rows*img.cols; k++) {
        const int i = k/img.cols;
        const int j = k%img.cols;
        const float x0 = j / scaleX + x1;
        const float y0 = i / scaleY + y1;
        const complex<float> z0(x0, y0);
        const uchar value = (uchar) mandelbrotFormula(z0, maxIter);
        img.data[k] = value;
    }
}

void mandelbrot_foreach(cv::Mat &img, const float x1, const float y1, 
        const float scaleX, const float scaleY, const int maxIter) {
    auto f = [&](uchar & pixel, const int position[]) {
        const int i = position[0];
        const int j = position[1];
        const float x0 = j / scaleX + x1;
        const float y0 = i / scaleY + y1;
        const complex<float> z0(x0, y0);
        const uchar value = (uchar) mandelbrotFormula(z0, maxIter);
        pixel = value;
    };
    img.forEach<uchar>(f);
}

class ParallelMandelbrot : public cv::ParallelLoopBody {
    public:
        ParallelMandelbrot (cv::Mat &img, const float x1, const float y1, 
                const float scaleX, const float scaleY, const int maxIter)
            : m_img(img), m_x1(x1), m_y1(y1), m_scaleX(scaleX), 
            m_scaleY(scaleY), m_maxIter(maxIter) {}
        virtual void operator ()(const cv::Range& range) const {
            for (int r = range.start; r < range.end; r++) {
                const int i = r / m_img.cols;
                const int j = r % m_img.cols;
                const float x0 = j / m_scaleX + m_x1;
                const float y0 = i / m_scaleY + m_y1;
                const complex<float> z0(x0, y0);
                const uchar value = (uchar) mandelbrotFormula(z0, m_maxIter);
                m_img.ptr<uchar>(i)[j] = value;
            }
        }
        ParallelMandelbrot& operator=(const ParallelMandelbrot &) {
            return *this;
        };
    private:
        cv::Mat &m_img;
        const float m_x1;
        const float m_y1;
        const float m_scaleX;
        const float m_scaleY;
        const int m_maxIter;
};

void mandelbrot_parallelfor(cv::Mat &img, const float x1, const float y1, 
        const float scaleX, const float scaleY, const int maxIter) {
    ParallelMandelbrot parallelMandelbrot(img, x1, y1, scaleX, scaleY, maxIter);
    parallel_for_(cv::Range(0, img.rows*img.cols), parallelMandelbrot);
}

/*
// opencv-3.3
void mandelbrot_parallelfor(cv::Mat &img, const float x1, const float y1, 
        const float scaleX, const float scaleY, const int maxIter) {
    function<void(const cv::Range&)> f = [&](const cv::Range & range) {
        for (int r = range.start; r < range.end; r++) {
            const int i = r / img.cols;
            const int j = r % img.cols;
            const float x0 = j / scaleX + x1;
            const float y0 = i / scaleY + y1;
            const complex<float> z0(x0, y0);
            const uchar value = (uchar) mandelbrotFormula(z0, maxIter);
            img.at<uchar>(i,j) = value;
        }
    };
    cv::parallel_for_(cv::Range(0, img.rows*img.cols), f);
}
*/

int main() {
    run_mandelbrot("omp_at", mandelbrot_omp_at);
    run_mandelbrot("omp_ptr", mandelbrot_omp_ptr);
    //run_mandelbrot("omp_data", mandelbrot_omp_data);
    run_mandelbrot("foreach", mandelbrot_foreach);
    run_mandelbrot("parallelfor", mandelbrot_parallelfor);
    run_mandelbrot("at", mandelbrot_at);
    run_mandelbrot("ptr", mandelbrot_ptr);
    //run_mandelbrot("data", mandelbrot_data);
    return 0;
}

