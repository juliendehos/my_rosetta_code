with import <nixpkgs> {};

pkgs.stdenv.mkDerivation {
  name = "checkerboard";
  buildInputs = [ 
    pkgs.opencv3
    pkgconfig
  ];
}
