# video processing

## description

threshold a video file :

![](input.png)

![](output.png)


## build & run

### threshold.cpp

```
g++ -std=c++11 -O2 `pkg-config --cflags --libs opencv` -o threshold threshold.cpp
./threshold
```

or (using [runcpp](https://github.com/juliendehos/runcpp)) :

```
runcpp threshold.cpp
```

### threshold_short.cpp

```
g++ -std=c++11 -O2 `pkg-config --cflags --libs opencv` -o threshold_short threshold_short.cpp
./threshold_short 
```

### threshold.py

```
./threshold.py
```

### threshold_short.py

```
python2 threshold_short.py
```

## stats

file                | nb lines | nb characters
--------------------|----------|---------------|
threshold.cpp       | 38       | 1059 
threshold.py        | 38       | 939
threshold_short.cpp | 28       | 860
threshold_short.py  | 24       | 688





