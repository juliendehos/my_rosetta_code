import cv2
import datetime

tp0 = datetime.datetime.now()
cap = cv2.VideoCapture('bmx.mkv')

width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
fps = cap.get(cv2.CAP_PROP_FPS)
fourcc = cv2.VideoWriter_fourcc(*'MJPG')
out = cv2.VideoWriter('output_py.avi', fourcc, fps, (width, height))

while(cap.isOpened()):
    ret, frame = cap.read()
    if not ret: break
    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    _, frame = cv2.threshold(frame, 130, 255, cv2.THRESH_BINARY);
    frame = cv2.cvtColor(frame, cv2.COLOR_GRAY2BGR)
    out.write(frame)

tp1 = datetime.datetime.now()
dt = tp1 - tp0
print dt.seconds + dt.microseconds*1e-6, "s"

