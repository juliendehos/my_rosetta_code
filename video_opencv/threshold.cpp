// RUNCPP_BUILD -std=c++11 -Wall -Wextra -O2 `pkg-config --cflags --libs opencv` 

#include <opencv2/opencv.hpp>
#include <chrono>

int main() {

    // start chrono
    auto tp0 = std::chrono::system_clock::now();

    // video capture
    cv::VideoCapture cap("bmx.mkv"); 

    // video writer
    int width = cap.get(CV_CAP_PROP_FRAME_WIDTH);
    int height = cap.get(CV_CAP_PROP_FRAME_HEIGHT);
    int fps = cap.get(CV_CAP_PROP_FPS);
    int fourcc = CV_FOURCC('M','J','P','G');
    cv::VideoWriter out("output_cpp.avi", fourcc, fps, cv::Size(width, height));

    // process video
    cv::Mat frame;
    while (true) {
        cap >> frame;
        if (frame.rows<=0 or frame.cols<=0) 
            break;
        cv::cvtColor(frame, frame, cv::COLOR_BGR2GRAY);
        cv::threshold(frame, frame, 130, 255, cv::THRESH_BINARY);
        cv::cvtColor(frame, frame, cv::COLOR_GRAY2BGR);
        out.write(frame);
    }

    // stop chrono
    auto tp1 = std::chrono::system_clock::now();
    std::cout << (tp1 - tp0).count()*1e-9 << " s\n";
    return 0;
}

