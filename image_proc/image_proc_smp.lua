require 'csvigo'

local normalize = function (img)
    min_img = torch.min(img)
    max_img = torch.max(img)
    return (img - min_img) / (max_img - min_img)
end

local compute_image = function (dirname, filename, extname, blur_size) 
    -- read image (grayscale, float64)
    local input_name = dirname .. '/' .. filename .. '.' .. extname
    print(input_name)
    local img_in = image.load(input_name, 1, 'double')

    -- blur image
    local kernel = torch.ones(blur_size, blur_size) / (blur_size*blur_size)
    local img_blur = image.convolve(img_in, kernel, 'same')

    -- tone mapped image
    local img_log = torch.log1p(img_in)

    -- output image
    local img_out = normalize(torch.add(img_log, 0.5*img_blur))

    --mul images
    local img_in_mul = normalize(img_in * img_in:t())

    -- output images
    image.save('out_' .. filename .. '_i0_lua.' .. extname, img_in)
    image.save('out_' .. filename .. '_i1_lua.' .. extname, img_out)
    image.save('out_' .. filename .. '_m0_lua.' .. extname, img_in_mul)
end

-- parameters
if #arg ~= 2 then
    print('waiting 2 parameters: data_csv blur_size')
    os.exit(-1)
end
local data_csv = arg[1]
local blur_size = tonumber(arg[2])

-- read data file
local metas = csvigo.load({path=data_csv, mode="raw", separator=" "})

local nb_cores = math.min(#metas, torch.getnumthreads())
print(data_csv, blur_size, nb_cores)

-- compute images 
local threads = require 'threads'
threads.Threads(
    nb_cores,
    function(i)
        require 'image'
    end,
    function(i)
        compute_image(metas[i][1], metas[i][2], metas[i][3], blur_size)
    end
)

