# nix-build
# nix-shell --pure --run "time ./result/bin/image_proc.out data_full.csv 201"
# nix-shell --pure --run "time python image_proc.py data_full.csv 201"
# nix-shell --pure --run "time octave image_proc.m data_full.csv 201"
# nix-shell --pure --run "time Rscript image_proc.R data_full.csv 201"

{ system ? builtins.currentSystem }:

let
    pkgs = import <nixpkgs> { inherit system; };

    myopencv = pkgs.opencv3.override { 
        enableOpenblas = true; 
    };
in

with pkgs;
stdenv.mkDerivation rec {
    name = "image_proc";
    buildInputs = [ 
        opencv3
        octave
        pkgconfig
        python36Packages.opencv3
        python36Packages.pandas
        R
        rPackages.EBImage
    ];
    src = ./.;
    buildPhase = ''
        gcc -o imgconv.out -std=c++11 -O2 `pkg-config --cflags opencv` imgconv.cpp `pkg-config --libs opencv`
    '';
    installPhase = ''
        mkdir -p $out/bin
        cp image_proc.out $out/bin/
    '';
}

