#include "fast-cpp-csv-parser/csv.h"
#include <opencv2/opencv.hpp>
#include <fstream>
#include <iostream>
#include <string>
#include <tuple>

using namespace std;
using Meta = tuple<string, string, string>;

void compute_image(const string & dirname, const string & filename, 
        const string & extname, int blur_size) {
    // read image (grayscale, float64)
    string input_name = dirname + "/" + filename + "." + extname;
    cout << input_name << endl;
    cv::Mat img_in; 
    cv::imread(input_name, cv::IMREAD_GRAYSCALE).convertTo(img_in, CV_64F);
    img_in /= 255.0;

    // blur image
    cv::Mat img_blur;
    cv::blur(img_in, img_blur, {blur_size, blur_size});

    // tone mapped image
    cv::Mat img_log = 1 + img_in;
    cv::log(img_log, img_log) ;

    // output image
    cv::Mat img_out = img_log + 0.5*img_blur;
    cv::normalize(img_out, img_out, 0, 1, cv::NORM_MINMAX);

    // mul images
    cv::Mat img_in_mul = img_in * img_in.t();
    cv::normalize(img_in_mul, img_in_mul, 0, 1, cv::NORM_MINMAX);

    // output images
    cv::imwrite("out_" + filename + "_i0_cpp." + extname, img_in*255.0);
    cv::imwrite("out_" + filename + "_i1_cpp." + extname, img_out*255.0);
    cv::imwrite("out_" + filename + "_m0_cpp." + extname, img_in_mul*255.0);
}

int main(int argc, char ** argv) {
    // parameters
    if (argc != 3) {
        cerr << "usage: " << argv[0] << " data_csv blur_size\n";
        exit(-1);
    }
    const string data_csv = argv[1];
    const int blur_size = atoi(argv[2]);
    cout << data_csv << ' ' << blur_size << endl;

    // read data file
    io::CSVReader<3, io::trim_chars<' '>, io::no_quote_escape<' '>> file(data_csv);
    file.set_header("dirname", "filename", "extname");
    string dirname, filename, extname;
    vector<Meta> metas;
    while(file.read_row(dirname, filename, extname))
        metas.push_back(Meta{dirname, filename, extname});

    // compute images 
#pragma omp parallel for
    for (unsigned i=0; i<metas.size(); i++) {
        string dirname, filename, extname;
        tie(dirname, filename, extname) = metas[i];
        compute_image(dirname, filename, extname, blur_size);
    }
    return 0;
}

