#!/usr/bin/env python3
import cv2, csv, numpy, pandas, sys

def compute_image(dirname, filename, extname, blur_size) :
    # read image (grayscale, float64)
    input_name = dirname + '/' + filename + '.' + extname
    print(input_name)
    img_in = cv2.imread(input_name, cv2.IMREAD_GRAYSCALE)
    img_in = img_in.astype(numpy.float64) / 255.0

    # blur image
    img_blur = cv2.blur(img_in, (blur_size, blur_size))

    # tone mapped image
    img_log = numpy.log1p(img_in)

    # output image
    img_out = img_log + 0.5*img_blur
    cv2.normalize(img_out, img_out, 0, 1, cv2.NORM_MINMAX)

    # output images
    cv2.imwrite('out_' + filename + '_i0_py.' + extname, img_in*255.0)
    cv2.imwrite('out_' + filename + '_i1_py.' + extname, img_out*255.0)

if __name__ == "__main__":
    # parameters
    if len(sys.argv) != 3:
        print('usage:', sys.argv[0], ' data_csv blur_size')
        sys.exit(-1)
    data_csv = sys.argv[1]
    blur_size = int(sys.argv[2])
    print(data_csv, blur_size)

    # read data file
    metas = pandas.read_csv(data_csv, delimiter=' ', header=None)

    # compute images 
    for _, dirname, filename, extname in metas.itertuples():
        compute_image(dirname, filename, extname, blur_size)

