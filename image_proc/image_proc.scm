(define-module (image_proc)
  #:use-module (guix gexp) 
  #:use-module (guix packages)
  #:use-module (gnu packages pkg-config)
  #:use-module (opencv) 
  #:use-module (guix build-system gnu)
  #:use-module (guix licenses))

(define-public image_proc
  (package
    (name "image_proc")
    (version "0.1")
    (source (local-file "" #:recursive? #t))
    (build-system gnu-build-system)
    (native-inputs `(("pkg-config" ,pkg-config)))
    (inputs `(("opencv" ,opencv)))
    (arguments '(#:phases (modify-phases %standard-phases (delete 'configure)) #:validate-runpath? #f #:tests? #f)) 
    (synopsis "image proc test")
    (description "Image proc test.")
    (home-page "http://www.example.org")
    (license gpl3+)))

