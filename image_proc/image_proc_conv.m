% parameters
if nargin != 2
    printf('usage: %s data_csv blur_size\n', program_name);
    exit;
endif
arg_list = argv();
data_csv = arg_list{1};
blur_size = str2num(arg_list{2});
printf('%s %d\n', data_csv, blur_size);

function res = normalize (img)
    min_img = min(min(img));
    max_img = max(max(img));
    res = (img - min_img) / (max_img - min_img);
endfunction

function compute_image(dirname, filename, extname, blursize)
    % read image 
    input_name = strcat(dirname, '/', filename, '.', extname);
    printf('%s\n', input_name);
    img_in = mean(imread(input_name), 3) / 255.0;  % TODO RGB to grayscale

    % blur image
    kernel = ones(blursize, blursize) / (blursize * blursize);
    img_blur = conv2 (img_in, kernel, 'same');

    % tone mapped image
    img_log = log1p(img_in);

    % output image
    img_out = normalize(img_log + 0.5*img_blur);

    % output images
    imwrite(img_in, strcat('out_', filename, '_i0_m.', extname));
    imwrite(img_out, strcat('out_', filename, '_i1_m.', extname));
endfunction

% read data file
[dirs, files, exts] = textread(data_csv, '%s %s %s');

% compute images 
for i = 1:length(files)
    compute_image(dirs{i}, files{i}, exts{i}, blur_size);
endfor

