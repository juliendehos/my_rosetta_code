#!/usr/bin/env python3
import cv2, csv, numpy, pandas, sys, joblib, os

def compute_image(dirname, filename, extname, blur_size) :
    # read image (grayscale, float64)
    input_name = dirname + '/' + filename + '.' + extname
    print(input_name)
    img_in = cv2.imread(input_name, cv2.IMREAD_GRAYSCALE)
    img_in = img_in.astype(numpy.float64) / 255.0

    # blur image
    img_blur = cv2.blur(img_in, (blur_size, blur_size))

    # tone mapped image
    img_log = numpy.log1p(img_in)

    # output image
    img_out = img_log + 0.5*img_blur
    cv2.normalize(img_out, img_out, 0, 1, cv2.NORM_MINMAX)

    # output images
    cv2.imwrite('out_' + filename + '_i0_py.' + extname, img_in*255.0)
    cv2.imwrite('out_' + filename + '_i1_py.' + extname, img_out*255.0)

if __name__ == "__main__":
    # parameters
    if len(sys.argv) != 3:
        print('usage:', sys.argv[0], ' data_csv blur_size')
        sys.exit(-1)
    data_csv = sys.argv[1]
    blur_size = int(sys.argv[2])
    nb_cores = len(os.sched_getaffinity(0))
    print(data_csv, blur_size, nb_cores)

    # read data file
    metas = pandas.read_csv(data_csv, delimiter=' ', header=None)

    # compute images 
    joblib.Parallel(n_jobs=nb_cores)(joblib.delayed(compute_image)(d, f, e, blur_size) for _, d, f, e in metas.itertuples())

