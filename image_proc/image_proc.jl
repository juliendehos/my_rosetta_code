using Images

function normalize(img)
    min_img = minimum(img)
    max_img = maximum(img)
    return (img - min_img) / (max_img - min_img)
end

function compute_image(dirname, filename, extname, blur_size) 
    # read image 
    input_name = string(dirname, "/", filename, ".", extname)
    println(input_name)
    img_in = normalize(convert(Array{Images.Gray}, load(input_name)))

    # blur image
    kernel = ones(blur_size, blur_size) / (blur_size*blur_size)
    img_blur = imfilter(img_in, centered(kernel))

    # tone mapped image
    img_log = log(1 + img_in)

    # output image
    img_out = normalize(img_log + 0.5*img_blur)
    
    # mul images
    img_in_mul = normalize(img_in * (transpose(img_in)))

    # output images
    save(string("out_", filename, "_i0_jl.", extname), img_in)
    save(string("out_", filename, "_i1_jl.", extname), img_out)
    save(string("out_", filename, "_m0_jl.", extname), img_in_mul)
end

function compute_images(data)
    for i = 1:size(data, 1)
        compute_image(data[i,1], data[i,2], data[i,3], blur_size)
    end
end

# parameters
if length(ARGS) != 2 
    println("waiting 2 parameters: data_csv blur_size")
    exit(-1)
end
const data_csv = ARGS[1]
const blur_size = parse(Int, ARGS[2])
println(data_csv, " ", blur_size)

# read data file
data = readdlm(data_csv, ' ', String)

# compute images 
compute_images(data)

