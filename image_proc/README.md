
# image_proc

## Description

- image_proc_conv: read 4k images, compute convolution, log1p, normalization
- image_proc: idem + matrix multiplication
- \*_smp: multithreading versions



## Build & run

```
git submodule init
git submodule update
make

# C++
time ./image_proc.out data.csv 201
...

# Python 3
time ./image_proc.py data.csv 201
...

# Torch 7
time th image_proc.lua data.csv 201
...

# Julia
time julia image_proc.jl data.csv 201
...

# Octave
time octave image_proc.m data.csv 201
...

# R (code written by Pierre-Alexandre Hébert)
R
source("https://bioconductor.org/biocLite.R")
biocLite("EBImage")
exit()
time ./image_proc.R data.csv 201
...

```

## Benchmarks

6-core CPU + Hyper-Threading

| code                            | time     | loc | code size |
|---------------------------------|----------|-----|-----------|
| image_proc.cpp                  |   1m43s  | 70  | 2099      |
| image_proc_smp.cpp              |     19s  | 71  | 2132      |
| image_proc.jl                   | 117m35s  | 50  | 1297      |
| image_proc.lua                  |  30m42s  | 51  | 1442      |
| image_proc_smp.lua              |   5m29s  | 59  | 1615      |
| image_proc.m                    |  37m32s  | 50  | 1381      |
| image_proc.py                   |   2m44s  | 45  | 1455      |
| image_proc_smp.py               |     37s  | 45  | 1524      |
| image_proc.R                    |   6m01s  | 59  | 1667      |

| code                            | time     | loc | code size |
|---------------------------------|----------|-----|-----------|
| image_proc_conv.cpp             |      3s2 | 65  | 1887      |
| image_proc_conv_smp.cpp         |      0s8 | 67  | 1973      |
| image_proc_conv.jl              |     37s  | 46  | 1151      |
| image_proc_conv.lua             |  30m15s  | 47  | 1292      |
| image_proc_conv_smp.lua         |   5m26s  | 55  | 1471      |
| image_proc_conv.m               |  35m39s  | 45  | 1221      |
| image_proc_conv.py              |      7s  | 40  | 1236      |
| image_proc_conv_smp.py          |      1s8 | 40  | 1305      |
| image_proc_conv.R               |   1m10s  | 54  | 1467      |


## TODO

- lua: optimize code
- julia: optimize code, fix matrix multiplication
- use the same blur method (1D separable convolutions ?)

## nix (opencv with openblas support)

```
nix-build config.nix -A image_proc -j12
time ./result/bin/image_proc.out data.csv 201
```

## python virtualenv

```
mkdir -p ~/opt
virtualenv3 -p /usr/bin/python3 --no-site-packages ~/opt/venv_opencv
source ~/opt/venv_opencv/bin/activate
pip install opencv-python pandas
time ./image_proc.py data.csv 201
deactivate
```

