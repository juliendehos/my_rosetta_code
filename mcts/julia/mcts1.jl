using Random

struct Game
    nMoves :: Int
    x0 :: Float64
    x1 :: Float64
end

function iToX(g::Game, iMove::Int)
    g.x0 + iMove * (g.x1 - g.x0) / g.nMoves
end

function playout(g::Game, iMove::Int, rng)
    x = randn(rng)*0.05 + 0.5
    xMove = iToX(g, iMove)
    abs(x - xMove) < 20*(g.x1 - g.x0) ? 1 : 0
end

mutable struct Mcts
    nWins :: Int
    nSims :: Int
    game :: Game
    lastI :: Int
    parent :: Union{Mcts, Nothing}
    children :: Vector{Mcts}
end

function Mcts(game::Game, parent::Union{Mcts,Nothing}=nothing)
    Mcts(0, 0, game, 0, parent, Vector{Mcts}(undef, game.nMoves))
end

isLeaf(mcts::Mcts) = mcts.lastI != mcts.game.nMoves

reward(mcts::Mcts) = mcts.nSims == 0 ? 0.0 : mcts.nWins / mcts.nSims

ucb1(rew, kuct, pSims, cSims) = rew + kuct * sqrt(log(pSims) / cSims)

function printMcts(levelMax::Int, level::Int, mcts::Mcts)
    println()
    for _ in 1:level
        print("  ")
    end
    print(" nw=", mcts.nWins)
    print(" ns=", mcts.nSims)
    if level >= levelMax
        print(" ...")
    else
        map(m -> printMcts(levelMax, level + 1, m), mcts.children)
    end
    if level == 0
        println()
    end
end

function bestMove(children::Vector{Mcts}, kuct::Float64, pSims::Int)
    scores = map(mcts -> ucb1(reward(mcts), kuct, pSims, mcts.nSims), children)
    (_, iBest) = findmax(scores)
    iBest
end

function iterateMcts!(mcts0::Mcts, kuct::Float64, rng)
    mcts = mcts0
    # selection
    while ! isLeaf(mcts)
        iBest = bestMove(mcts.children, kuct, mcts.nSims)
        mcts = mcts.children[iBest]
    end
    # expansion
    iMove = mcts.lastI
    game = mcts.game
    childGame = Game(game.nMoves, iToX(game, iMove), iToX(game, iMove+1))
    mcts.children[iMove+1] = Mcts(childGame, mcts)
    mcts.lastI += 1
    # simulation
    incWins = playout(childGame, iMove, rng)
    # backpropagation
    backMcts = mcts.children[iMove+1]
    while backMcts != nothing
        backMcts.nWins += incWins
        backMcts.nSims += 1
        backMcts = backMcts.parent
    end
end

function run!(game::Game, kuct::Float64, nbIterations::Int, rng)
    mcts = Mcts(game)
    for _ in 1:nbIterations
        iterateMcts!(mcts, kuct, rng)
    end
    mcts
end

function main()
    rng = MersenneTwister()
    levelMax = 1
    kuct = 0.1
    # nbIterations = 20
    nbIterations = 1000000

    @time mcts = run!(Game(10, 0.0, 1.0), kuct, nbIterations, rng)
    printMcts(levelMax, 0, mcts)
end

main()


