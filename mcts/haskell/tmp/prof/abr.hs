
import           Control.Monad
import           Data.List (foldl')
import           System.Random.MWC

data Tree = Leaf | Node Int Tree Tree

newNode :: Int -> Tree
newNode e = Node e Leaf Leaf

insert :: Tree -> Int -> Tree
insert Leaf e0 = newNode e0
insert n@(Node e l r) e0 
    | e0 == e = n
    | e0 < e = Node e (insert l e0) r
    | otherwise = Node e l (insert r e0)

search :: Tree -> Int -> Bool
search Leaf _ = False
search (Node e l r) e0 
    | e0 == e = True
    | e0 < e = search l e0
    | otherwise = search r e0

myrun2 :: GenIO -> IO [Bool]
myrun2 gen = do
    xs1 <- replicateM 1000000 (uniformR (0,10000000) gen)
    xs2 <- replicateM 1000000 (uniformR (0,10000000) gen) 
    let t0 = newNode 42
        t1 = foldl' insert t0 xs1
        t2 = map (search t1) xs2
    return t2

main :: IO ()
main = createSystemRandom >>= myrun2 >>= print . length . filter id


{-

-- version "immutable"

import           Control.Monad
import           Data.List (foldl')
import qualified Data.Vector as V
import           System.Random.MWC

data Tree = Leaf | Node Int (V.Vector Tree)

newNode :: Int -> Tree
newNode e = Node e $ V.replicate 2 Leaf

insert :: Tree -> Int -> Tree
insert Leaf e0 = newNode e0
insert n@(Node e c) e0 
    | e0 == e = n
    | otherwise = 
        let ci = if e0<e then 0 else 1
            cc = c V.! ci
            cc' = insert cc e0
            c' = c V.// [(ci, cc')]
        in Node e c'

search :: Tree -> Int -> Bool
search Leaf _ = False
search (Node ei ci) e0 
    | e0 == ei = True
    | otherwise = 
        let i = if e0<ei then 0 else 1
            c = ci V.! i
        in search c e0

myrun2 :: GenIO -> IO [Bool]
myrun2 gen = do
    xs1 <- replicateM 1000000 (uniformR (0,10000000) gen)
    xs2 <- replicateM 1000000 (uniformR (0,10000000) gen) 
    let t0 = newNode 42
        t1 = foldl' insert t0 xs1
        t2 = map (search t1) xs2
    return t2

main :: IO ()
main = createSystemRandom >>= myrun2 >>= print . length . filter id


-- version "mutable"

import           Control.Monad
import           Control.Monad.ST
import qualified Data.Vector.Mutable as MV
import           System.Random.MWC

data Tree s = Leaf | Node Int (MV.STVector s (Tree s))

newNode :: Int -> ST s (Tree s)
newNode e = Node e <$> MV.replicate 2 Leaf

insert :: Tree s -> Int -> ST s ()
insert Leaf _ = error "empty tree"
insert (Node e c) e0 = 
    when (e0 /= e) $ do
        let ci = if e0<e then 0 else 1
        cc <- MV.read c ci
        case cc of
            Leaf -> newNode e0 >>= MV.write c ci
            cn -> insert cn e0

search :: Tree s -> Int -> ST s Bool
search Leaf _ = return False
search (Node ei ci) e0 
    | e0 == ei = return True
    | otherwise = do
        let i = if e0<ei then 0 else 1
        c <- MV.read ci i
        search c e0

myrun2 :: GenST s -> ST s [Bool]
myrun2 gen = do
    t <- newNode 42
    replicateM_ 1000000 (uniformR (0,10000000) gen >>= insert t)
    replicateM 1000000 (uniformR (0,10000000) gen >>= search t)

main :: IO ()
main = createSystemRandom >>= stToIO . myrun2 >>= print . length . filter id


-}


