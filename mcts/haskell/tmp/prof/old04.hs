-- {-# LANGUAGE BangPatterns #-}

import           Control.Monad.ST
import qualified Data.Vector as V
import qualified Data.Vector.Mutable as M
import           System.Random.MWC
import           System.Random.MWC.Distributions

data Game = Game 
    { _nMoves :: !Int
    , _x0 :: !Double
    , _x1 :: !Double } deriving Show

iToX :: Game -> Int -> Double
iToX (Game nMoves x0 x1) iMove = 
    x0 + fromIntegral iMove * (x1 - x0) / fromIntegral nMoves

playout :: Game -> GenST s -> Int -> ST s Int
playout g@(Game _ x0 x1) gen iMove = do
    xi <- standard gen
    let x = xi*0.05 + 0.5
        xMove = iToX g iMove 
    return $ if abs (x-xMove) < 20*(x1-x0) then 1 else 0

data Mcts s = Mcts 
    { _nWins :: !Int  -- TODO STRef ?
    , _nSims :: !Int  -- TODO STRef ?
    , _game :: !Game
    , _lastI :: !Int  -- TODO STRef ?
    , _children :: M.STVector s (Mcts s) }

showMcts :: Int -> Int -> Mcts s -> ST s String
showMcts levelMax level (Mcts nWins nSims _ _ children)
    | level >= levelMax = return $ nodestr ++ " ..."
    | otherwise = do
        vect <- V.freeze children
        str <- concat . V.toList <$> mapM (showMcts levelMax (level+1)) vect
        return $ nodestr ++ str
    where nodestr = "\n" ++ concat (replicate level "  ")
                         ++ " nw=" ++ show nWins 
                         ++ " ns=" ++ show nSims

newMcts :: Int -> Int -> Game -> ST s (Mcts s)
newMcts nWins nSims game = Mcts nWins nSims game 0 <$> M.new (_nMoves game)

isLeaf :: Mcts s -> Bool
isLeaf (Mcts _ _ game lastI _) = lastI < _nMoves game

reward :: Mcts s -> Double
reward (Mcts nw ns _ _ _) = 
    if ns == 0 then 0.0 else fromIntegral nw / fromIntegral ns

ucb1 :: Double -> Double -> Int -> Int -> Double
ucb1 rew kuct pSims cSims = 
    rew + kuct * sqrt (log (fromIntegral pSims) / fromIntegral cSims)

bestMove :: M.STVector s (Mcts s) -> Double -> Int -> ST s Int
bestMove cs kuct pSims = do
    -- vect <- V.unsafeFreeze cs
    vect <- V.freeze cs
    (ib1, _) <- V.ifoldM' fAcc (-1, -1) vect
    return $! ib1
    where fAcc (ib, sb) i m = let s = ucb1 (reward m) kuct pSims (_nSims m)
                              in return $ if s>sb then (i, s) else (ib, sb)

{-

bestMove :: M.STVector s (Mcts s) -> Double -> Int -> ST s Int
bestMove cs kuct pSims = do
    vect <- V.unsafeFreeze cs
    return $! V.maxIndex $ V.map (\m -> ucb1 (reward m) kuct pSims (_nSims m)) vect

bestMove :: M.STVector s (Mcts s) -> Double -> Int -> ST s Int
bestMove cs kuct pSims = do
    -- TODO unsafe, foldM
    vect <- V.freeze cs
    let (_, ib1, _) = V.foldl' fAcc (0, 0, -1) vect
    return ib1
    where fScore mcts = ucb1 (reward mcts) kuct pSims (_nSims mcts)
          fAcc (i, ib, sb) m = let !s = fScore m
                               in if s>sb then (i+1, i, s) else (i+1, ib, sb)

-}

iterateMcts :: Mcts s -> Double -> GenST s -> ST s (Mcts s, Int)
iterateMcts mcts@(Mcts nWins nSims game lastI children0) kuct gen
    | isLeaf mcts = do
        -- simulation
        let nMoves = _nMoves game
        let game1 = Game nMoves (iToX game lastI) (iToX game (lastI+1)) 
        incWins <- playout game1 gen nMoves
        -- expansion
        child <- newMcts incWins 1 game1
        M.write children0 lastI child
        -- update
        let mcts' = Mcts (nWins+incWins) (nSims+1) game (lastI+1) children0
        return (mcts', incWins)
    | otherwise = do
        -- selection
        iBest <- bestMove children0 kuct nSims 
        mBest <- M.read children0 iBest
        (mBest', incWins) <- iterateMcts mBest kuct gen -- TODO
        M.write children0 iBest mBest'
        -- backpropagation
        let mcts' = mcts { _nWins = nWins+incWins, _nSims = nSims+1 }
        return (mcts', incWins)

run :: Double -> Int -> GenST s -> Mcts s -> ST s (Mcts s)
run _ 0 _ m = return m
run kuct n gen m = do
    (m', _) <- iterateMcts m kuct gen
    run kuct (n-1) gen m'

main :: IO ()
main = do
    result <- withSystemRandom $ \gen -> do
        mcts0 <- newMcts 0 0 game0
        mcts1 <- run kuct nbIterations gen mcts0
        showMcts levelMax 0 mcts1
    putStrLn result
    where kuct = 0.1
          levelMax = 1
          nbIterations = 1000000
          game0 = Game 10 0.0 1.0

