import           Data.Vector (freeze)
import qualified Data.Vector.Mutable as M

foo :: M.IOVector Int -> IO ()
foo v = M.write v 0 (2::Int)

main :: IO ()
main = do
    v <- M.new 1
    foo v
    iv <- freeze v
    print iv

