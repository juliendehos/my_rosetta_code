
import           Control.Monad
import           Data.List (foldl')
import qualified Data.Vector as V
import           System.Random.MWC

data Tree = Leaf | Node Int (V.Vector Tree) deriving Show

newNode :: Int -> Tree
newNode e = Node e $ V.replicate 2 Leaf

insert :: Tree -> Int -> Tree
insert Leaf e0 = newNode e0
insert n@(Node e c) e0 
    | e0 == e = n
    | otherwise = 
        let ci = if e0<e then 0 else 1
            cc = c V.! ci
            cc' = insert cc e0
            c' = c V.// [(ci, cc')]
        in Node e c'

search :: Tree -> Int -> Bool
search Leaf _ = False
search (Node ei ci) e0 
    | e0 == ei = True
    | otherwise = 
        let i = if e0<ei then 0 else 1
            c = ci V.! i
        in search c e0

myrun1 :: [Bool]
myrun1 = 
    let t0 = newNode 13
        t1 = foldl' insert t0 [12, 37, 42]
    in map (search t1) [12, 11, 42, 14] 

myrun2 :: GenIO -> IO [Bool]
myrun2 gen = do
    xs1 <- replicateM 1000000 (uniformR (0,10000000) gen)
    xs2 <- replicateM 1000000 (uniformR (0,10000000) gen) 
    let t0 = newNode 42
        t1 = foldl' insert t0 xs1
        t2 = map (search t1) xs2
    return t2

main :: IO ()
main = do
    gen <- createSystemRandom
    print myrun1
    (myrun2 gen) >>= print . length . filter id

