{-# LANGUAGE BangPatterns #-}

import Control.Lens ((&), (.~), element)
import Control.Monad.Primitive
import qualified Data.Vector as V
import System.Random.MWC
import System.Random.MWC.Distributions

data Game = Game 
    { _nMoves :: !Int
    , _x0 :: !Double
    , _x1 :: !Double } deriving Show

iToX :: Game -> Int -> Double
iToX (Game nMoves x0 x1) iMove = 
    x0 + fromIntegral iMove * (x1 - x0) / fromIntegral nMoves

playout :: PrimMonad m => Game -> Gen (PrimState m) -> Int -> m Int
playout g@(Game _ x0 x1) gen iMove = do
    xi <- standard gen
    let x = xi*0.05 + 0.5
        xMove = iToX g iMove 
    return $ if abs (x-xMove) < 20*(x1-x0) then 1 else 0

data Mcts = Mcts 
    { _nWins :: !Int
    , _nSims :: !Int
    , _game :: !Game
    , _lastI :: !Int
    , _children :: V.Vector Mcts }

showMcts :: Int -> Int -> Mcts -> String
showMcts levelMax level (Mcts nw ns _ _ c) = 
    "\n" ++ concat (replicate level "  ")
    ++ " nw=" ++ show nw 
    ++ " ns=" ++ show ns
    ++ if level >= levelMax then " ..." 
        else concatMap (showMcts levelMax (level + 1)) c

isLeaf :: Mcts -> Bool
isLeaf (Mcts _ _ game lastI _) = lastI < _nMoves game

reward :: Mcts -> Double
reward (Mcts nw ns _ _ _) = 
    if ns == 0 then 0.0 else fromIntegral nw / fromIntegral ns

ucb1 :: Double -> Double -> Int -> Int -> Double
ucb1 rew kuct pSims cSims = 
    rew + kuct * sqrt (log (fromIntegral pSims) / fromIntegral cSims)

bestMove :: V.Vector Mcts -> Double -> Int -> Int
bestMove cs kuct pSims = ib1
    where fScore mcts = ucb1 (reward mcts) kuct pSims (_nSims mcts)
          fAcc (i, ib, sb) m = let !s = fScore m
                               in if s>sb then (i+1, i, s) else (i+1, ib, sb)
          (_, ib1, _) = V.foldl' fAcc (0, 0, -1) cs

iterateMcts :: PrimMonad m => Mcts -> Double -> Gen (PrimState m) -> m (Mcts, Int)
iterateMcts mcts@(Mcts nWins nSims game lastI children0) kuct gen
    | isLeaf mcts = do
        let nmoves = _nMoves game
            -- expansion
            cs1 = V.map  mkMcts (V.enumFromTo 0 nmoves)
            mkMcts i = Mcts 0 0 (Game nmoves (iToX game i) (iToX game (i+1)) ) 0 V.empty
            children1 = if V.null children0 then cs1 else children0
        -- simulation
        incWins <- playout (_game $ children1 V.! lastI) gen nmoves
        let childMcts = (children1 V.! lastI) { _nWins = incWins, _nSims = 1 }
            mcts' = Mcts (nWins+incWins) (nSims+1) game (lastI+1)
                        (V.update children1 (V.singleton (lastI, childMcts)))
        return (mcts', incWins)
    | otherwise = do
        let iBest = bestMove children0 kuct nSims
            -- selection
            mBest = children0 V.! iBest
        (mBest', incWins) <- iterateMcts mBest kuct gen
        -- backpropagation
        let mcts' = mcts { _nWins = (nWins+incWins)
                         , _nSims = (nSims+1) 
                         , _children = (children0 & element iBest .~ mBest') }
        return (mcts', incWins)

run :: PrimMonad m => Double -> Int -> Gen (PrimState m) -> (Mcts, Int) -> m (Mcts, Int)
run _ 0 _ mi = return mi
run kuct n gen (m, _) = iterateMcts m kuct gen >>= run kuct (n-1) gen

main :: IO ()
main = do
    gen <- createSystemRandom
    (mcts1, _) <- run kuct nbIterations gen (mcts0, 0)
    putStrLn $ showMcts levelMax 0 mcts1
    where kuct = 0.1
          levelMax = 1
          nbIterations = 1000000 :: Int
          mcts0 = Mcts 0 0 (Game 10 0.0 1.0) 0 V.empty

