
import           Data.STRef
import           Control.Monad.ST

addST :: STRef s Int -> STRef s Int -> ST s Int
addST x y = do
    x' <- readSTRef x
    y' <- readSTRef y
    return $ x' + y'

main :: IO ()
main = do

    let r = runST $ do
                x <- newSTRef 20
                y <- newSTRef 22
                addST x y
    print r

    r2 <- stToIO $ do
                x <- newSTRef 20
                y <- newSTRef 22
                addST x y

    print r2

