-- {-# LANGUAGE BangPatterns #-}

import           Control.Monad.ST
import qualified Data.Vector as V
import           Data.STRef
import qualified Data.Vector.Mutable as M
import           System.Random.MWC
import           System.Random.MWC.Distributions

data Game = Game 
    { _nMoves :: !Int
    , _x0 :: !Double
    , _x1 :: !Double } deriving Show

iToX :: Game -> Int -> Double
iToX (Game nMoves x0 x1) iMove = 
    x0 + fromIntegral iMove * (x1 - x0) / fromIntegral nMoves

playout :: Game -> GenST s -> Int -> ST s Int
playout g@(Game _ x0 x1) gen iMove = do
    xi <- standard gen
    let x = xi*0.05 + 0.5
        xMove = iToX g iMove 
    return $ if abs (x-xMove) < 20*(x1-x0) then 1 else 0

data Mcts s = Mcts 
    { _nWinsR :: !(STRef s Int)
    , _nSimsR :: !(STRef s Int)
    , _game :: !Game
    , _lastIR :: !(STRef s Int)
    , _children :: M.STVector s (Mcts s) }

showMcts :: Int -> Int -> Mcts s -> ST s String
showMcts levelMax level (Mcts nWinsR nSimsR _ _ children) = do
    nWins <- readSTRef nWinsR
    nSims <- readSTRef nSimsR
    let nodestr = "\n" ++ concat (replicate level "  ")
                       ++ " nw=" ++ show nWins 
                       ++ " ns=" ++ show nSims
    if level >= levelMax then return $ nodestr ++ " ..."
    else do
        vect <- V.freeze children
        str <- concat . V.toList <$> mapM (showMcts levelMax (level+1)) vect
        return $ nodestr ++ str

newMcts :: Int -> Int -> Game -> ST s (Mcts s)
newMcts nWins nSims game = do
    nWinsR <- newSTRef nWins
    nSimsR <- newSTRef nSims
    lastIR <- newSTRef 0
    children <- M.new (_nMoves game)
    return $ Mcts nWinsR nSimsR game lastIR children

isLeaf :: Int -> Int -> Bool
isLeaf nMoves lastI = lastI < nMoves

reward :: Int -> Int -> Double
reward nWins nSims = 
    if nWins == 0 then 0.0 else fromIntegral nWins / fromIntegral nSims

ucb1 :: Double -> Double -> Int -> Int -> Double
ucb1 rew kuct pSims cSims = 
    rew + kuct * sqrt (log (fromIntegral pSims) / fromIntegral cSims)

{-

bestMove :: M.STVector s (Mcts s) -> Double -> Int -> ST s Int
bestMove cs kuct pSims = do
    -- vect <- V.unsafeFreeze cs
    vect <- V.freeze cs
    return $! V.maxIndex $ V.map (\m -> ucb1 (reward m) kuct pSims (_nSims m)) vect

-}

bestMove :: M.STVector s (Mcts s) -> Double -> Int -> ST s Int
bestMove cs kuct pSims = do
    -- vect <- V.unsafeFreeze cs
    vect <- V.freeze cs
    (ib1, _) <- V.ifoldM' fAcc (-1, -1) vect
    return $! ib1
    where fAcc (ib, sb) i m = do
            nWins <- readSTRef (_nWinsR m)
            nSims <- readSTRef (_nSimsR m)
            let s = ucb1 (reward nWins nSims) kuct pSims nSims
            return $ if s>sb then (i, s) else (ib, sb)

iterateMcts :: Mcts s -> Double -> GenST s -> ST s Int
iterateMcts (Mcts nWinsR nSimsR game lastIR children0) kuct gen = do
    nSims <- readSTRef nSimsR
    lastI <- readSTRef lastIR
    incWins <- if isLeaf (_nMoves game) lastI
        then do
            -- simulation
            let nMoves = _nMoves game
            let game1 = Game nMoves (iToX game lastI) (iToX game (lastI+1)) 
            incWins0 <- playout game1 gen nMoves
            -- expansion
            child <- newMcts incWins0 1 game1
            M.write children0 lastI child
            -- update
            modifySTRef lastIR (+1)
            return incWins0
        else do
            -- selection
            iBest <- bestMove children0 kuct nSims 
            mBest <- M.read children0 iBest
            iterateMcts mBest kuct gen
            -- M.write children0 iBest mBest'
    -- backpropagation
    modifySTRef nWinsR (+incWins)
    modifySTRef nSimsR (+1)
    return incWins

run :: Double -> Int -> GenST s -> Mcts s -> ST s (Mcts s)
run _ 0 _ m = return m
run kuct n gen m = do
    _ <- iterateMcts m kuct gen
    run kuct (n-1) gen m

main :: IO ()
main = do
    result <- withSystemRandom $ \gen -> do
        mcts0 <- newMcts 0 0 game0
        mcts1 <- run kuct nbIterations gen mcts0
        showMcts levelMax 0 mcts1
    putStrLn result
    where kuct = 0.1
          levelMax = 1
          nbIterations = 1000000
          game0 = Game 10 0.0 1.0

