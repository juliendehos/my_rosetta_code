with import <nixpkgs> {};

mkShell {
  buildInputs = [
    llvm_6

    (haskellPackages.ghcWithPackages (ps: with ps; [
      base
      lens
      mwc-random
      vector

      mtl
      deepseq
      primitive
      random
    ]))
  ];

}

