import           Control.Monad (when)
import           Control.Monad.ST
import qualified Data.Vector as V
import qualified Data.Vector.Mutable as M
import           System.Random.MWC
import           System.Random.MWC.Distributions

data Game = Game 
    { _nMoves :: !Int
    , _x0 :: !Double
    , _x1 :: !Double } deriving Show

iToX :: Game -> Int -> Double
iToX (Game nMoves x0 x1) iMove = 
    x0 + fromIntegral iMove * (x1 - x0) / fromIntegral nMoves

playout :: Game -> GenST s -> Int -> ST s Int
playout g@(Game _ x0 x1) gen iMove = do
    xi <- standard gen
    let x = xi*0.05 + 0.5
        xMove = iToX g iMove 
    return $ if abs (x-xMove) < 20*(x1-x0) then 1 else 0

data Mcts s = Mcts 
    { _nWins :: !Int
    , _nSims :: !Int
    , _game :: !Game
    , _lastI :: !Int
    , _children :: M.STVector s (Mcts s) }

printMcts :: Int -> Int -> Double -> Int -> Mcts RealWorld -> IO ()
printMcts levelMax level kUct pSims m@(Mcts nWins nSims _ _ children) = do
    putStr $ "\n" ++ concat (replicate (level-1) " |  ")
    when (level /= 0) $ putStr " |- "
    putStr $ "nWins=" ++ show nWins
           ++ " nSims=" ++ show nSims
    when (level /= 0) $ putStr $ " score=" ++ show (ucb1 m kUct pSims)
    if (level >= levelMax)
    then putStr " ..."
    else V.freeze children >>= V.mapM_ (printMcts levelMax (level+1) kUct nSims)

newMcts :: Int -> Int -> Game -> ST s (Mcts s)
newMcts nWins nSims game = Mcts nWins nSims game 0 <$> M.new (_nMoves game)

isLeaf :: Mcts s -> Bool
isLeaf (Mcts _ _ game lastI _) = lastI < _nMoves game

reward :: Mcts s -> Double
reward (Mcts nw ns _ _ _) = 
    if ns == 0 then 0.0 else fromIntegral nw / fromIntegral ns

ucb1 :: Mcts s -> Double -> Int -> Double
ucb1 m kUct pSims = 
    reward m + kUct * sqrt (log (fromIntegral pSims) / fromIntegral (_nSims m))

bestMove :: M.STVector s (Mcts s) -> Double -> Int -> ST s Int
bestMove cs kUct pSims = do
    vect <- V.freeze cs
    let (ib1, _) = V.ifoldl' fAcc (-1, -1) vect
    return $! ib1
    where fAcc (ib, sb) i m = let s = ucb1 m kUct pSims
                              in if s>sb then (i, s) else (ib, sb)

iterateMcts :: Mcts s -> Double -> GenST s -> ST s (Mcts s, Int)
iterateMcts mcts@(Mcts nWins nSims game lastI children0) kUct gen = 
    if isLeaf mcts then do
        -- simulation
        let nMoves = _nMoves game
        let game1 = Game nMoves (iToX game lastI) (iToX game (lastI+1)) 
        incWins <- playout game1 gen nMoves
        -- expansion
        child <- newMcts incWins 1 game1
        M.write children0 lastI child
        -- update
        let mcts' = Mcts (nWins+incWins) (nSims+1) game (lastI+1) children0
        return (mcts', incWins)
    else do
        -- selection
        iBest <- bestMove children0 kUct nSims 
        mBest <- M.read children0 iBest
        (mBest', incWins) <- iterateMcts mBest kUct gen
        M.write children0 iBest mBest'
        -- backpropagation
        let mcts' = mcts { _nWins = nWins+incWins, _nSims = nSims+1 }
        return (mcts', incWins)

run :: Double -> Int -> GenST s -> Mcts s -> ST s (Mcts s)
run _ 0 _ m = return m
run kUct n gen m = do
    (m', _) <- iterateMcts m kUct gen
    run kUct (n-1) gen m'

main :: IO ()
main = do
    let kUct = 0.1
        nbIterations = 1000000
    mcts1 <- withSystemRandom $ \gen -> do
        mcts0 <- newMcts 0 0 (Game 10 0.0 1.0)
        run kUct nbIterations gen mcts0
    printMcts 1 0  kUct 0 mcts1
    putStrLn ""

