import           Control.Monad 
import qualified Data.Vector.Mutable as MV
import           System.Random

data Tree = Leaf | Node Int (MV.IOVector Tree)

newNode :: Int -> IO Tree
newNode e = Node e <$> MV.replicate 2 Leaf

insert :: Tree -> Int -> IO ()
insert Leaf _ = error "empty tree"
insert (Node e c) e0 = 
    when (e0 /= e) $ do
        let ci = if e0<e then 0 else 1
        cc <- MV.read c ci
        case cc of
            Leaf -> newNode e0 >>= MV.write c ci
            cn -> insert cn e0

search :: Tree -> Int -> IO Bool
search Leaf _ = return False
search (Node ei ci) e0 
    | e0 == ei = return True
    | otherwise = do
        let i = if e0<ei then 0 else 1
        c <- MV.read ci i
        search c e0

main :: IO ()
main = do
    t <- newNode 42
    replicateM_ 1000000 (randomRIO (0,10000000) >>= insert t)
    res <- replicateM 1000000 (randomRIO (0,10000000) >>= search t)
    print $ take 10 res

    -- mapM_ (insert t) [12, 37, 42]
    -- mapM (search t) [12, 11, 42, 14] >>= print

