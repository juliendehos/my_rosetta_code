{-# LANGUAGE BangPatterns #-}

import           Data.STRef
import           Control.Monad
import           Control.Monad.ST
import qualified Data.Vector.Mutable as MV
import           System.Random.MWC

data Tree s = Leaf | Node !(STRef s Int) (MV.STVector s (Tree s))

newNode :: Int -> ST s (Tree s)
newNode e = do
    er <- newSTRef e
    cr <- MV.replicate 2 Leaf
    return $ Node er cr

insert :: Tree s -> Int -> ST s ()
insert Leaf _ = error "empty tree"
insert (Node er cr) e0 = do
    e <- readSTRef er
    when (e0 /= e) $ do
        let ci = if e0<e then 0 else 1
        cc <- MV.read cr ci
        case cc of
            Leaf -> newNode e0 >>= MV.write cr ci
            cn -> insert cn e0

search :: Tree s -> Int -> ST s Bool
search Leaf _ = return False
search (Node eri cri) e0 = do
    ei <- readSTRef eri
    if e0 == ei 
    then return True
    else do
        let i = if e0<ei then 0 else 1
        c <- MV.read cri i
        search c e0

myrun1 :: ST s [Bool]
myrun1 = do
    t <- newNode 13
    mapM_ (insert t) [12, 37, 42]
    mapM (search t) [12, 11, 42, 14] 

myrun2 :: GenST s -> ST s [Bool]
myrun2 gen = do
    t <- newNode 42
    replicateM_ 1000000 (uniformR (0,10000000) gen >>= insert t)
    replicateM 1000000 (uniformR (0,10000000) gen >>= search t)

main :: IO ()
main = do
    gen <- createSystemRandom
    stToIO myrun1 >>= print
    stToIO (myrun2 gen) >>= print . length . filter id

