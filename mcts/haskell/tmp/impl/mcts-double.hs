{-# LANGUAGE BangPatterns #-}

import Control.Lens ((&), (.~), element)
import System.Random.MWC
import Control.Monad.Primitive
import System.Random.MWC.Distributions

data Game = Game 
    { _nMoves :: !Double
    , _x0 :: !Double
    , _x1 :: !Double } deriving Show

iToX :: Game -> Int -> Double
iToX (Game nMoves x0 x1) iMove = 
    x0 + fromIntegral iMove * (x1 - x0) / nMoves
{-# INLINE iToX #-}

playout :: PrimMonad m => Game -> Gen (PrimState m) -> Int -> m Double
playout g@(Game _ x0 x1) gen iMove = do
    xi <- standard gen
    let x = xi*0.05 + 0.5
        xMove = iToX g iMove 
    return (if abs (x-xMove) < 20*(x1-x0) then 1 else 0)
{-# INLINE playout #-}

data Mcts = Mcts 
    { _nWins :: !Double
    , _nSims :: !Double
    , _game :: !Game
    , _children :: [Mcts] }

showMcts :: Int -> Int -> Mcts -> String
showMcts levelMax level (Mcts nw ns _ c) = 
    "\n" ++ concat (replicate level "  ")
    ++ " nw=" ++ show nw 
    ++ " ns=" ++ show ns
    ++ if level >= levelMax then " ..." 
        else concatMap (showMcts levelMax (level + 1)) c

isLeaf :: Mcts -> Bool
isLeaf (Mcts _ _ game children) = length children /= truncate (_nMoves game)
{-# INLINE isLeaf #-}

reward :: Mcts -> Double
reward (Mcts nw ns _ _) = 
    if ns == 0 then 0.0 else nw / ns
{-# INLINE reward #-}

ucb1 :: Double -> Double -> Double -> Double -> Double
ucb1 rew kuct pSims cSims = 
    rew + kuct * sqrt (log pSims) / cSims
{-# INLINE ucb1 #-}

bestMove :: [Mcts] -> Double -> Double -> Int
bestMove children kuct pSims = snd $ maximum $ zip (map fScore children) [0..]
    where fScore mcts = ucb1 (reward mcts) kuct pSims (_nSims mcts)
{-# INLINE bestMove #-}

iterateMcts :: PrimMonad m => Mcts -> Double -> Gen (PrimState m) -> m (Mcts, Double)
iterateMcts mcts@(Mcts nWins nSims game children) kuct gen
    | isLeaf mcts = do
        let iMove = length children
            -- expansion
            childGame = Game (_nMoves game) (iToX game iMove) (iToX game (iMove+1))
        -- simulation
        incWins <- playout childGame gen iMove
        let childMcts = Mcts incWins 1 childGame []
            mcts' = Mcts (nWins+ incWins) (nSims+1) game (children++[childMcts])
        return (mcts', incWins)
    | otherwise = do
            let iBest = bestMove children kuct nSims
                -- selection
                mBest = children !! iBest
            (mBest', incWins) <- iterateMcts mBest kuct gen
            -- backpropagation
            let children' = children & element iBest .~ mBest'
                mcts' = Mcts (nWins+incWins) (nSims+1) game children'
            return (mcts', incWins)

run :: PrimMonad m => Double -> Int -> Gen (PrimState m) -> (Mcts, Double) -> m (Mcts, Double)
run _ 0 _ mi = return mi
run kuct n gen (m, _) = iterateMcts m kuct gen >>= run kuct (n-1) gen

main :: IO ()
main = do
    gen <- createSystemRandom
    (mcts1, _) <- run kuct nbIterations gen (mcts0, 0)
    putStrLn $ showMcts levelMax 0 mcts1
    where kuct = 0.1
          levelMax = 1
          nbIterations = 1000000 :: Int
          mcts0 = Mcts 0 0 (Game 10 0.0 1.0) []

