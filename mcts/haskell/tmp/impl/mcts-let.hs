import Control.Lens ((&), (.~), element)
import System.Random (getStdGen, StdGen)
import Data.Random.Normal (normal)

data Game = Game 
    { _nMoves :: Int
    , _x0 :: Double
    , _x1 :: Double } deriving Show

iToX :: Game -> Int -> Double
iToX (Game nMoves x0 x1) iMove = x0 + dx * fromIntegral iMove
    where dx = (x1 - x0) / fromIntegral nMoves

playout :: Game -> StdGen -> Int -> (Int, StdGen)
playout g@(Game _ x0 x1) rng iMove = (r, rng')
    where (xi, rng') = normal rng
          x = xi*0.05 + 0.5
          xMove = iToX g iMove 
          r = if abs (x-xMove) < 20*(x1-x0) then 1 else 0

data Mcts = Mcts 
    { _nWins :: Int
    , _nSims :: Int
    , _game :: Game
    , _children :: [Mcts] }

showMcts :: Int -> Int -> Mcts -> String
showMcts levelMax level (Mcts nw ns _ c) = 
    "\n" ++ concat (replicate level "  ")
    ++ " nw=" ++ show nw 
    ++ " ns=" ++ show ns
    ++ if level >= levelMax then " ..." 
        else concatMap (showMcts levelMax (level + 1)) c

isLeaf :: Mcts -> Bool
isLeaf (Mcts _ _ game children) = length children /= _nMoves game

reward :: Mcts -> Double
reward (Mcts nw ns _ _) = 
    if ns == 0 then 0.0 else fromIntegral nw / fromIntegral ns

ucb1 :: Double -> Double -> Int -> Int -> Double
ucb1 rew kuct pSims cSims = 
    rew + kuct * sqrt (log (fromIntegral pSims) / fromIntegral cSims)

bestMove :: [Mcts] -> Double -> Int -> Int
bestMove children kuct pSims = snd $ maximum $ zip (map fScore children) [0..]
    where fScore mcts = ucb1 (reward mcts) kuct pSims (_nSims mcts)

iterateMcts :: Mcts -> Double -> StdGen -> (Mcts, Int, StdGen)
iterateMcts mcts@(Mcts nWins nSims game children) kuct rng
    | isLeaf mcts = 
        let iMove = length children
            -- expansion
            childGame = Game (_nMoves game) (iToX game iMove) (iToX game (iMove+1))
            -- simulation
            (incWins, rng') = playout childGame rng iMove
            childMcts = Mcts incWins 1 childGame []
            mcts' = Mcts (nWins+incWins) (nSims+1) game (children++[childMcts])
            -- mcts' = Mcts (nWins+incWins) (nSims+1) game (childMcts:children)
        in (mcts', incWins, rng')
    | otherwise = 
            let iBest = bestMove children kuct nSims
                -- selection
                mBest = children !! iBest
                (mBest', incWins, rng') = iterateMcts mBest kuct rng
                -- backpropagation
                children' = children & element iBest .~ mBest'
                mcts' = Mcts (nWins+incWins) (nSims+1) game children'
            in (mcts', incWins, rng')

run :: Double -> Int -> (Mcts, Int, StdGen) -> (Mcts, Int, StdGen)
run _ 0 mir = mir
run kuct n (m, _, r) = run kuct (n-1) (iterateMcts m kuct r)

main :: IO ()
main = do
    rng <- getStdGen
    let kuct = 0.1
        nbIterations = 1000000 :: Int
        mcts0 = Mcts 0 0 (Game 10 0.0 1.0) []
        (mcts1, _, _) = run kuct nbIterations (mcts0, 0, rng)
        levelMax = 1
    putStrLn $ showMcts levelMax 0 mcts1

