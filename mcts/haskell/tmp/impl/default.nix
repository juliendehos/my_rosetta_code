{ pkgs ? import <nixpkgs> {} }:

let
  #drv = pkgs.haskell.packages.ghc884.callCabal2nix "mcts" ./. {};
  drv = pkgs.haskellPackages.callCabal2nix "mcts" ./. {};
in

if pkgs.lib.inNixShell then drv.env else drv

