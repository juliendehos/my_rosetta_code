
struct Vec2 {
    x: f64,
    y: f64,
}


fn print_vec2(v: Vec2) {
    let Vec2 { x, y } = v;
    println!("Vec2 {} {}", x, y);
}

fn main() {

    let v1 = Vec2 { x: 1.0, y: 3.0 };
    print_vec2(v1);

    let v2 = Vec2 { x: 14.0, y: 4.2 };
    let v3 = Vec2 { x: 14.0, ..v2 };
    print_vec2(v3);

    println!("v3.y: {}", v2.y);

}


