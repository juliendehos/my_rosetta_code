#include <algorithm>
#include <iostream>
#include <random>
#include <vector>

using namespace std;

class Random {
    private:
        mt19937_64 _engine;
        // uniform_real_distribution<double> _distribution;
        normal_distribution<double> _distribution;
    public:
        Random() : _engine(random_device{}()), _distribution(0, 1) {}
        Random(const Random &) = delete;
        double operator()() { return _distribution(_engine); }
};

struct Game {
    int _nMoves;
    double _x0;
    double _x1;

    double iToX(int iMove) const {
        const double dx = (_x1 - _x0) / double(_nMoves);
        return _x0 + dx*iMove;
    }

    int playout(Random & rng, double xMove) const {
        const double x = rng()*0.05 + 0.5;
        return abs(x - xMove) < 20*(_x1 - _x0) ? 1 : 0;
    }

};

struct Mcts {
    int _nWins;
    int _nSims;
    double _kuct;
    Game _game;
    Mcts * _ptrParent;
    vector<Mcts> _children;

    Mcts(double kuct, const Game & game, Mcts * ptrParent = nullptr) : 
        _nWins(0), _nSims(0), _kuct(kuct), _game(game), _ptrParent(ptrParent) {
        _children.reserve(_game._nMoves);
    }

    bool isLeaf() const {
        return _children.size() != unsigned(_game._nMoves);
    }

    double reward() const {
        return _nSims==0 ? 0.0 : double(_nWins)/double(_nSims);
    }

    double UCB1() const {
        const double exploitation = reward();
        const double exploration = sqrt(log(_ptrParent->_nSims) / _nSims);
        return exploitation + _kuct * exploration;
    }

    int bestMove() {
        double sBest = -1;
        int iBest = -1;
        for (int i=0; i<_game._nMoves; ++i) {
            double s = _children[i].UCB1();
            if (s > sBest) {
                sBest = s;
                iBest = i;
            }
        }
        return iBest;
    }

    void print(int index, int level, int levelMax) {
        cout << endl;
        for (int l=0; l<level-1; l++)
            cout << " |  ";
        if (level != 0)
            cout << " |- ";
        cout << "index=" << index 
            << " nwins=" << _nWins
            << " nsims=" << _nSims;
        if (level != 0)
            cout << " score=" << UCB1();
        if (level >= levelMax) 
            cout << " ..." ;
        else
            for (unsigned i=0; i<_children.size(); i++) 
                _children[i].print(i, level+1, levelMax);
    }

};

void iterate(Mcts & mcts, Random & rng) {
    Mcts * ptr = &mcts;
    // selection
    while (not ptr->isLeaf()) {
        const int iBest = ptr->bestMove();
        ptr = &(ptr->_children[iBest]);
    }
    const int iMove = ptr->_children.size();
    const int nMoves = ptr->_game._nMoves;
    const double xMove0 = ptr->_game.iToX(iMove);
    const double xMove1 = ptr->_game.iToX(iMove+1);
    // expansion
    Game game {nMoves, xMove0, xMove1};
    ptr->_children.emplace_back(ptr->_kuct, game, ptr);
    Mcts * newPtr = &(ptr->_children.back());
    // simulation
    int incNwins = newPtr->_game.playout(rng, xMove0);
    // backpropagation
    while (newPtr) {
        newPtr->_nWins += incNwins;
        newPtr->_nSims += 1;
        newPtr = newPtr->_ptrParent;
    }
}

int main() {
    const double kuct = 0.1;
    const int nbIterations = 1000000;
    Random rng;
    Game game {10, 0.0, 1.0};
    Mcts mcts(kuct, game);
    for (int n=0; n<nbIterations; n++)
        iterate(mcts, rng);
    mcts.print(0, 0, 1);
    return 0;
}

