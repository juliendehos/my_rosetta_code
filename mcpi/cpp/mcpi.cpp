#include <chrono>
#include <cmath>
#include <iostream>
#include <random>

using namespace std;

float mcpi(int nbSamples, mt19937 & engine) {
    uniform_real_distribution<float> distribution(0, 1);
    int nbDisk = 0;
    for (int i=0; i<nbSamples; i++) {
        const float x = distribution(engine); 
        const float y = distribution(engine); 
        if (x*x + y*y < 1)
            nbDisk++;
    }
    return 4.0 * nbDisk / float(nbSamples);
}

int main(int argc, char ** argv) {

    if (argc != 2) {
        cout << "usage: <nb samples>" << endl;
        exit(-1);
    }
    const int nbSamples = atoi(argv[1]);
    mt19937 engine(random_device{}());

    const auto t0 = chrono::system_clock::now();
    const float myPi = mcpi(nbSamples, engine);
    const auto t1 = chrono::system_clock::now();
    const chrono::duration<float> dt = t1 - t0;
    const float err = fabs(M_PI - myPi);

    cout << dt.count() << endl;
    cout << err << endl;

    return 0;
}

// make
// ./mcpi 100000000

