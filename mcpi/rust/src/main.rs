extern crate rand;
use rand::Rng;
use std::env;
use std::time::{Instant};
use std::f32::consts::{PI};

fn mcpi(mut rng : rand::ThreadRng, nb_samples : u32) -> f32 {
    let mut nb_disk : u32 = 0;
    for _ in 1..nb_samples {
        let x = rng.gen::<f32>();
        let y = rng.gen::<f32>();
        if x*x + y*y < 1.0 {
            nb_disk += 1;
        }
    }
    return 4.0 * (nb_disk as f32) / (nb_samples as f32);
}

fn main() {
    if env::args().len() != 2 {
        println!("usage: <nb samples>");
    }
    else {
        let nb_samples: u32 = env::args().nth(1).unwrap().parse().unwrap();
        let rng = rand::thread_rng();
        let t0 = Instant::now();
        let my_pi = mcpi(rng, nb_samples);
        let t1 = Instant::now();
        let err = (PI - my_pi).abs();
        println!("{:?}", t1.duration_since(t0));
        println!("{}", err);
    }
}

