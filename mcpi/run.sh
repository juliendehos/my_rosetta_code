#!/bin/sh

NB_SAMPLES="100000000"

echo "*** C++ ***"
make -C cpp 
./cpp/mcpi ${NB_SAMPLES}
echo ""

echo "*** Julia ***"
julia ./jit/mcpi.jl ${NB_SAMPLES}
echo ""

echo "*** JavaScript ***"
node ./jit/mcpi.js ${NB_SAMPLES}
echo ""

echo "*** Lua ***"
luajit ./jit/mcpi.lua ${NB_SAMPLES}
echo ""

echo "*** Rust ***"
cd rust
cargo +stable build --release 
./target/release/mcpi ${NB_SAMPLES}
cd ..
echo ""

echo "*** Haskell ***"
cd haskell
stack build
stack exec mcpi ${NB_SAMPLES}
cd ..
echo ""

