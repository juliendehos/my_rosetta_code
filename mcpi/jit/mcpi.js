"use strict";

const { _, performance } = require('perf_hooks');

function mcpi(nbSamples) {
    let nbDisk = 0;
    for (let i=0; i<nbSamples; i++) {
        const x = Math.random();
        const y = Math.random();
        if (x*x + y*y < 1.0)
            nbDisk += 1;
    }
    return 4.0 * nbDisk / nbSamples
}

if (process.argv.length != 3) {
    console.log("usage: <nb samples>");
}
else {
    const nbSamples = Number(process.argv[2]);
    const t0 = performance.now();
    const myPi = mcpi(nbSamples);
    const t1 = performance.now();
    const dt = (t1 - t0)*0.001;
    const err = Math.abs(Math.PI - myPi);
    console.log(dt);
    console.log(err);
}

// node mcpi.js 100000000

