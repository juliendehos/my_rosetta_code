using Random

function mcpi(nbSamples, rng)
    nbDisk = 0
    for i in 1:nbSamples
        x = rand(rng)
        y = rand(rng)
        if x*x + y*y < 1.0
            nbDisk += 1
        end
    end
    4.0 * nbDisk / nbSamples
end

if !isinteractive()
    if length(ARGS) == 1
        nbSamples = parse(Int, ARGS[1])
        rng = MersenneTwister()
        @time myPi = mcpi(nbSamples, rng)
        err = abs(pi - myPi)
        println(err)
    else 
        println("usage: <nb samples>")
    end
end

# julia mcpi.jl 100000000

