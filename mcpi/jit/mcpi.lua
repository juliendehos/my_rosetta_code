function mcpi(nbSamples)
    local nbDisk = 0
    for i = 1, nbSamples do
        local x = math.random()
        local y = math.random()
        if x*x + y*y < 1.0 then
            nbDisk = nbDisk + 1
        end
    end
    return 4.0 * nbDisk / nbSamples
end

if #arg ~= 1 then
    print("usage: <nb samples>")
else
    local nbSamples = arg[1]
    local t0 = os.clock()
    local myPi = mcpi(nbSamples)
    local t1 = os.clock()
    local dt = t1 - t0
    local err = math.abs(math.pi - myPi)
    print(dt)
    print(err)
end

-- luajit mcpi.lua 100000000
