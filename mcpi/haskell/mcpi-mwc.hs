{-# LANGUAGE ScopedTypeVariables #-}
import System.Clock
import System.Environment
import System.Random.MWC
import Control.Monad.Primitive

mc :: PrimMonad m => Gen (PrimState m) -> Int -> Int -> Int -> m Float
mc _ s 0 nbSamples = return $ 4.0 * fromIntegral s / fromIntegral nbSamples
mc gen s n nbSamples = do
    x :: Float <- uniform gen 
    y :: Float <- uniform gen 
    let r = if x*x + y*y < 1.0 then 1 else 0
    mc gen (s+r) (n-1) nbSamples

run nbSamples gen = mc gen 0 nbSamples nbSamples

main = do
    args <- getArgs
    if length args /= 1
    then putStrLn "usage: <nb samples>"
    else do
        let nbSamples = read $ head args
        gen <- createSystemRandom
        t0 <- getTime Monotonic
        myPi <- run nbSamples gen 
        t1 <- getTime Monotonic
        print $ diffTimeSpec t0 t1
        print $ abs $ pi - myPi
