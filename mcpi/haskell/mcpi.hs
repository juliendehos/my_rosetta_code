import Control.Exception
import System.Clock
import System.Environment
import System.Random.Mersenne.Pure64

mc :: Int -> Int -> Int -> PureMT -> Double
mc s 0 nbSamples _ = 4.0 * fromIntegral s / fromIntegral nbSamples
mc s n nbSamples rng0 = mc (s+r) (n-1) nbSamples rng2
    where (x, rng1) = randomDouble rng0
          (y, rng2) = randomDouble rng1
          r = if x*x + y*y < 1.0 then 1 else 0

run nbSamples = mc 0 nbSamples nbSamples

main = do
    args <- getArgs
    if length args /= 1
    then putStrLn "usage: <nb samples>"
    else do
        let nbSamples = read $ head args
        gen <- newPureMT
        t0 <- getTime Monotonic
        myPi <- evaluate $ run nbSamples gen 
        t1 <- getTime Monotonic
        print $ diffTimeSpec t0 t1
        print $ abs $ pi - myPi

{-
mc :: Int -> Int -> Int -> PureMT -> Double
mc s 0 nbSamples _ = 4.0 * fromIntegral s / fromIntegral nbSamples
mc s n nbSamples rng0 = 
    case randomDouble rng0 of
        (x, rng1) -> case randomDouble rng1 of
            (y, rng2) -> mc (s+r) (n-1) nbSamples rng2
                where r = if x*x + y*y < 1.0 then 1 else 0
-}
