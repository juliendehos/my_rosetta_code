#!/usr/bin/env python2
import os, numpy, skimage, skimage.io 

max_imgs = 2000
training_dir = 'data/nist64/training'
test_dir = 'data/nist64/test'
nb_pixels = 64*64

# model
labels = os.listdir(training_dir)
nb_classes = len(labels)
log_priors = numpy.zeros((nb_classes,1), float)
log_likelyhoods = numpy.zeros((nb_classes,nb_pixels), float)

# training
print 'label nb_imgs_k max_logLikelyhood'
for k, label in enumerate(os.listdir(training_dir)):
    label_path = os.path.join(training_dir, label)
    sum_img = numpy.zeros(nb_pixels)
    for img_file in os.listdir(label_path):
        img_path = os.path.join(label_path, img_file)
        img = skimage.io.imread(img_path, as_grey=True).reshape(nb_pixels)
        sum_img += numpy.ones(nb_pixels, float) - skimage.img_as_float(img)
        log_priors[k] += 1
        if log_priors[k] >= max_imgs:
            break
    sum_img += numpy.ones(nb_pixels)
    log_likelyhoods[k,:] = numpy.log(sum_img / numpy.sum(sum_img))
    print label, log_priors[k,0], max(log_likelyhoods[k,:])
log_priors = numpy.log(log_priors / numpy.sum(log_priors))

# test
nb_tests = 0
hs_tests = 0
print '\nfilename truth result'
for test_file in os.listdir(test_dir):
    test_path = os.path.join(test_dir, test_file)
    img = skimage.io.imread(test_path, as_grey=True).reshape(nb_pixels,1)
    test_img = numpy.ones((nb_pixels,1), float) - skimage.img_as_float(img)
    scores = log_priors + numpy.dot(log_likelyhoods, test_img)
    best_k = numpy.argmax(scores)
    result = labels[best_k]
    truth = test_file[7]
    print test_file, truth, result
    nb_tests += 1
    if (truth != result):
        hs_tests += 1
print '\nerror rate:', float(hs_tests)/float(nb_tests)

