// gcc -shared -fPIC -Wall -Wextra -O2 -lz -lpng -o my_png.so my_png.c
// gcc -fPIC -Wall -Wextra -O2 -lz -lpng -o my_png.out my_png.c

#include <png.h>

int my_png_read(const char * filename, double * out_data, unsigned img_width, unsigned img_height) {

    FILE * fp = fopen(filename, "rb");
    if (fp == 0) {
        fprintf(stderr, "%s %d %s\n", __FILE__, __LINE__, filename);
        return -1;
    }

    png_byte header[8];
    size_t header_size = fread(header, 1, 8, fp);
    if (header_size != 8) {
        fprintf(stderr, "%s %d %s\n", __FILE__, __LINE__, filename);
        fclose(fp);
        return -1;
    }

    if (png_sig_cmp(header, 0, 8)) {
        fprintf(stderr, "%s %d %s\n", __FILE__, __LINE__, filename);
        fclose(fp);
        return -1;
    }

    png_structp png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    if (!png_ptr) {
        fprintf(stderr, "%s %d %s\n", __FILE__, __LINE__, filename);
        fclose(fp);
        return -1;
    }

    png_infop info_ptr = png_create_info_struct(png_ptr);
    if (!info_ptr) {
        fprintf(stderr, "%s %d %s\n", __FILE__, __LINE__, filename);
        png_destroy_read_struct(&png_ptr, (png_infopp)NULL, (png_infopp)NULL);
        fclose(fp);
        return -1;
    }

    if (setjmp(png_jmpbuf(png_ptr))) {
        png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
        fclose(fp);
        return -1;
    }

    png_init_io(png_ptr, fp);

    png_set_sig_bytes(png_ptr, 8);

    int transforms 
        = PNG_TRANSFORM_EXPAND | PNG_TRANSFORM_STRIP_16 | PNG_TRANSFORM_PACKING 
        | PNG_TRANSFORM_STRIP_ALPHA | PNG_TRANSFORM_GRAY_TO_RGB; 
    png_read_png(png_ptr, info_ptr, transforms, NULL);

    unsigned width = png_get_image_width(png_ptr, info_ptr);
    if (width != img_width) {
        fprintf(stderr, "%s %d %s\n", __FILE__, __LINE__, filename);
        return -1;
    }

    unsigned height = png_get_image_height(png_ptr, info_ptr);
    if (height != img_height) {
        fprintf(stderr, "%s %d %s\n", __FILE__, __LINE__, filename);
        return -1;
    }

    unsigned channels = png_get_channels(png_ptr, info_ptr);
    if (channels != 3) {
        fprintf(stderr, "%s %d %s\n", __FILE__, __LINE__, filename);
        return -1;
    }

    png_bytepp rows = png_get_rows(png_ptr, info_ptr);
    for (unsigned i=0; i<height; i++)
        for (unsigned j=0; j<width; j++)
            out_data[i*width + j] = rows[i][j*3]/255.0;

    png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
    fclose(fp);
    return 0;
}

