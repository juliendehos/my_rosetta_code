function read_png(filename, width, height)
    data = Array{Float64,1}(width*height)
    ccall((:my_png_read, "my_png"), Cint, (Cstring, Ref{Cdouble}, Cuint, Cuint), filename, data, width, height)
    return data
end

function main()
    const max_imgs = 2000
    const training_dir = "../data/nist64/training"
    const test_dir = "../data/nist64/test"
    const width = 64
    const height = 64
    const nb_pixels = width*height

    # model
    const labels = readdir(training_dir)
    const nb_classes = length(labels)
    log_priors = zeros(nb_classes)
    log_likelyhoods = zeros(nb_classes, nb_pixels)
    # training
    println("label nb_imgs_k max_logLikelyhood")
    for (k,label) in enumerate(labels)
        dir_path = training_dir * "/" * label * "/"
        sum_img = zeros(1, nb_pixels)
        for filename in readdir(dir_path)
            sum_img += 1.0 - reshape(read_png(dir_path * filename, width, height), 1, nb_pixels)
            log_priors[k] += 1.0
            if log_priors[k] >= max_imgs
                break
            end
        end
        sum_img += 1
        log_likelyhoods[k,:] = log(sum_img / sum(sum_img))
        println(label, " ", log_priors[k], " ", max(log_likelyhoods[k]))
    end
    log_priors = log(log_priors / sum(log_priors))

    # test
    nb_tests = 0
    hs_tests = 0
    println("\nfilename truth result")
    for test_file in readdir(test_dir)
        test_img = 1.0 - reshape(read_png(test_dir * "/" * test_file, width, height), nb_pixels, 1)
        scores = log_priors + (log_likelyhoods * test_img)
        best_k = indmax(scores)
        truth = string(test_file[8])
        result = labels[best_k]
        println(test_file, " ", truth, " ", result)
        nb_tests += 1
        if truth != result
            hs_tests += 1
        end
    end
    println("\nerror rate: ", hs_tests / nb_tests)
end

main()

