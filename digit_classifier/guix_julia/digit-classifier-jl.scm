(define-module (digit-classifier-jl)
  #:use-module (guix gexp) 
  #:use-module (guix packages)
  #:use-module (gnu packages julia)
  #:use-module (gnu packages image)
  #:use-module (gnu packages zip)
  #:use-module (guix build-system gnu)
  #:use-module (guix licenses))

(define-public digit-classifier-jl
  (package
    (name "digit-classifier-jl")
    (version "0.2")
    (source (local-file "" #:recursive? #t))
    (build-system gnu-build-system)
    (arguments '(#:phases (modify-phases %standard-phases 
                                         (delete 'configure)
                                         (delete 'check))))
    (inputs `(("julia" ,julia)
              ("image" ,libpng)
              ("zip" ,unzip)))
    (synopsis "handwritten digit classifier")
    (description "Handwritten digit classifier.")
    (home-page #f)
    (license #f)))

