# digit_classifier

## description

multimodal naive bayesian classifier (for handwritten digit classification)

## prepare data

```
tar zxf data.tar.gz
```


## run code

### c++

```
# install g++, pkg-config, make, opencv
make
./digit_classifier.out
```

### julia

- ImageMagick (slow):

```
julia
Pkg.add("Images")
Pkg.add("ImageMagick")
exit()
julia digit_classifier.jl
```

- JuliaOpenCV (slow): <https://github.com/JuliaOpenCV/OpenCV.jl>

```
julia digit_classifier_cv.jl
```

- libpng (fast):

```
make
julia digit_classifier_libpng.jl
```


### lua/torch

```
# install torch and luarocks
luarocks install image
th digit_classifier.lua
```


### octave

```
./digit_classifier.m
```


### python

- debian : python-sklearn python-skimage
- pip : scikit-learn scikit-image

```
./digit_classifier.py
```

### R

code written by Pierre-Alexandre Hébert

```
R
install.packages("png")
quit()
./digit_classifier.R
```


## comparison

| code                        | versions                 | time  | loc | code size |
|-----------------------------|--------------------------|-------|-----|-----------|
| digit_classifier.cpp        | gcc 6.3.0, opencv 3.2    | 1.5   | 71  | 2997      |
| digit_classifier.jl         | julia v0.5               | 217.7 | 49  | 1340      |
| digit_classifier_cv.jl      | julia v0.5, opencv 3.2   | 174.1 | 53  | 1521      |
| digit_classifier_libpng.jl  | julia v0.5, libpng 1.6   | 3.8   | 58  | 1867      |
| digit_classifier.lua        | torch7 (4f5ad0f)         | 4.3   | 54  | 1667      |
| digit_classifier.m          | octave 4.2               | 66.6  | 51  | 1432      |
| digit_classifier.py         | python 2.7.13            | 6.6   | 49  | 1704      |
| digit_classifier.R          | R 3.3.2                  | 1.5   | 52  | 1407      |

