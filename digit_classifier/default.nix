with import <nixpkgs> {}; 

stdenv.mkDerivation {

    name = "digit_classifier";

    buildInputs = [ 
        pkgconfig boost opencv3 libpng gtk
    ];

    src = ./. ;

    installPhase = ''
        mkdir -p $out/bin
        cp *.out *.so $out/bin/
    '';

}

