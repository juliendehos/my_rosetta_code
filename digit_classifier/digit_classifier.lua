require 'image'

max_imgs = 2000
training_dir = 'data/nist64/training'
test_dir = 'data/nist64/test'
nb_pixels = 64*64

-- model
labels = {}
for dir in paths.iterdirs(training_dir) do 
    table.insert(labels, dir) 
end
nb_classes = #labels
log_priors = torch.Tensor(nb_classes):zero()
log_likelyhoods = torch.Tensor(nb_classes, nb_pixels):zero()

-- training
print('label\tnb_imgs_k\tmax_logLikelyhood')
for k, label in pairs(labels) do
    local dir_path = training_dir .. '/' .. label .. '/'
    local sum_img = torch.Tensor(1, nb_pixels):zero()
    for filename in paths.iterfiles(dir_path) do
        local file_path = dir_path .. filename
        local img = (image.load(file_path, 1, 'double')):reshape(1, nb_pixels)
        sum_img = sum_img + (1 - img)
        log_priors[k] = log_priors[k] + 1
        if log_priors[k] >= max_imgs then 
            break 
        end
        log_likelyhoods[k] = torch.log((1 + sum_img) / sum_img:sum())
    end
    print(label, log_priors[k], log_likelyhoods[k]:max())
end
log_priors = torch.log(log_priors / log_priors:sum())

-- test
nb_tests = 0
hs_tests = 0
print('\nfilename\ttruth\tresult')
for filename in paths.iterfiles(test_dir) do
    local file_path = test_dir .. '/' .. filename
    local img = 1 - (image.load(file_path, 1, 'double')):reshape(nb_pixels, 1)
    local scores = log_priors + log_likelyhoods * img
    _, best_k = torch.max(scores, 1)
    local result = labels[best_k[1]]
    local truth = string.sub(filename, 8, 8)
    print(filename, truth, result)
    nb_tests = nb_tests + 1
    if truth ~= result then
        hs_tests = hs_tests + 1
    end
end
print('\nerror rate:', hs_tests/nb_tests)

