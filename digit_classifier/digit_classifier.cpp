#include <boost/filesystem.hpp>
#include <boost/range/iterator_range.hpp>
#include <opencv2/opencv.hpp>

using namespace std;
namespace fs = boost::filesystem;

int main() {

    const unsigned max_imgs = 2000;
    const string training_dir = "data/nist64/training";
    const string test_dir = "data/nist64/test";
    const unsigned nb_pixels = 64*64;

    // model
    vector<string> labels;
    for(const auto & label_entry : boost::make_iterator_range(fs::directory_iterator(training_dir), {}))
        labels.emplace_back(label_entry.path().stem().string());
    const unsigned nb_classes = labels.size();
    cv::Mat log_priors = cv::Mat::zeros(nb_classes, 1, CV_32F);
    cv::Mat log_likelyhoods = cv::Mat::zeros(nb_classes, nb_pixels, CV_32F);

    // training
    cout << endl << "label nbImages maxLogLikelyhood" << endl;
    for (unsigned k=0; k<nb_classes; k++) {
        const string & label = labels[k];
        cv::Mat sum_img = cv::Mat::zeros(1, nb_pixels, CV_32F);
        for(const auto & img_entry : boost::make_iterator_range(fs::directory_iterator(training_dir + "/" + label), {})) { 
            string filename = img_entry.path().string();
            cv::Mat image;
            cv::imread(filename, cv::IMREAD_GRAYSCALE).reshape(1, 1).convertTo(image, CV_32F);
            cv::threshold(image, image, 127.0, 1.0, cv::THRESH_BINARY_INV);
            sum_img += image;
            log_priors.at<float>(k)++;
            if (log_priors.at<float>(k) >= max_imgs) 
                break;
        }
        cv::Mat log_likelyhood = sum_img + cv::Mat::ones(1, nb_pixels, CV_32F);
        log_likelyhood /= cv::sum(log_likelyhood)[0];
        cv::log(log_likelyhood, log_likelyhood);
        log_likelyhood.copyTo(log_likelyhoods.row(k));
        double max_val;
        cv::minMaxLoc(log_likelyhood, nullptr, &max_val, nullptr, nullptr);
        cout << labels[k] << " " << log_priors.at<float>(k) << " " << max_val << endl;
    }
    cv::log(log_priors/cv::sum(log_priors)[0], log_priors);

    // test
    unsigned hs_tests = 0;
    unsigned nb_tests = 0;
    cout << "\nfilename truth result" << endl;
    for(const auto & img_entry : boost::make_iterator_range(fs::directory_iterator(test_dir), {})) { 
        string filename = img_entry.path().string();
        cv::Mat image;
        cv::imread(filename, cv::IMREAD_GRAYSCALE).reshape(1, nb_pixels).convertTo(image, CV_32F);
        cv::threshold(image, image, 127.0, 1.0, cv::THRESH_BINARY_INV);
        cv::Mat scores = log_priors + log_likelyhoods * image;
        cv::Point best_p;
        cv::minMaxLoc(scores, nullptr, nullptr, nullptr, &best_p);
        char result = labels[best_p.y][0];
        string stem = img_entry.path().stem().string();
        char truth = stem[7];
        cout << stem << " " << truth << " " << result << endl;
        if (truth != result) 
            hs_tests++;
        nb_tests++;
    }
    cout << "\nerror rate: " << float(hs_tests) / float(nb_tests) << endl;
    return 0;
}

