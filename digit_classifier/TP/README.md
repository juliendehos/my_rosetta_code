
# Reconnaissance de chiffres avec un classifieur Bayésien

## Algo

bayes + additive smoothing


## Parcours de fichiers avec boost::filesystem

cf classe `path` (section decomposition) : 
<http://www.boost.org/doc/libs/1_55_0/libs/filesystem/doc/reference.html#class-path>


## Images et matrices avec OpenCV

**cf la doc** : <http://docs.opencv.org/3.2.0/>

- créer une matrice initialisée à zéro : `zeros`
- créer une matrice initialisée à un : `ones`
- lire une image dans une matrice (penser à convertir en niveau de gris en même temps) : `imread`
- reformater une matrice en un vecteur : `reshape``
- convertir une matrice de `unsigned char` en matrice de double : `convertTo`
- ajouter, soustraire des matrices : opérateurs ` + += - `
- multiplier, diviser une matrice par un réel : ` * / /= `
- produit matriciel : ` * ` 
- accéder à un coefficient d'une matrice : `at`
- calculer la somme des éléments d'une matrice : `sum` (attention : retourne un `Scalar`)
- calculer le log des coefficient d'une matrice : `log`
- accéder à une ligne d'une matrice : `row`
- copier une matrice (ou sous-matrice) dans une autre matrice (ou sous-matrice) : `copyTo`
- récupérer le min et le max d'une matrice (et leur position) : `minMaxLoc`

