#include <boost/filesystem.hpp>
#include <boost/range/iterator_range.hpp>
#include <opencv2/opencv.hpp>

using namespace std;
namespace fs = boost::filesystem;

struct MnbModel {
    vector<string> _labels;
    unsigned _nbClasses;
    unsigned _nbPixels;
    cv::Mat _logPriors;
    cv::Mat _logLikelyhoods;
};

int main() {

    const string TRAINING_DIR = "data/nist64/training";
    const string TEST_DIR = "data/nist64/test";
    const unsigned NB_PIXELS = 64*64;

    // init model
    MnbModel model;
    for(const auto & labelEntry : boost::make_iterator_range(fs::directory_iterator(TRAINING_DIR), {})) {
        model._labels.emplace_back(labelEntry.path().stem().string());
    }
    model._nbClasses = model._labels.size();
    model._nbPixels = NB_PIXELS;
    model._logPriors = cv::Mat::zeros(model._nbClasses, 1, CV_64F);
    model._logLikelyhoods = cv::Mat::zeros(model._nbClasses, model._nbPixels, CV_64F);

    // train model
    cout << "\nlabel nbImages maxLogLikelyhood" << endl;
    // TODO

    // test
    unsigned hsTests = 0;
    unsigned nbTests = 0;
    cout << "\nfilename truth result" << endl;
    // TODO
    cout << "\nerror rate: " << double(hsTests) / double(nbTests) << endl;

    return 0;
}

