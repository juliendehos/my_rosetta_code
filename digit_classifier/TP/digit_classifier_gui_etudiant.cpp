#include <boost/filesystem.hpp>
#include <boost/range/iterator_range.hpp>
#include <opencv2/opencv.hpp>

using namespace std;
namespace fs = boost::filesystem;

//////////////////////////////////////////////////////////////////////
// multimodal naive bayesian model
//////////////////////////////////////////////////////////////////////

struct MnbModel {
    vector<string> _labels;
    unsigned _nbClasses;
    unsigned _nbPixels;
    cv::Mat _logPriors;
    cv::Mat _logLikelyhoods;
};

//MnbModel initAndTrainModel(const string & pathname, unsigned nbPixels) {
MnbModel initAndTrainModel(const string &, unsigned) {

    // init model
    MnbModel model;
    // TODO

    // train model
    // TODO

    return model;
}

//pair<int,cv::Mat> predictImage(const MnbModel & model, const cv::Mat & image) {
pair<int,cv::Mat> predictImage(const MnbModel &, const cv::Mat &) {

    // TODO
    cv::Mat scores;
    int bestLabel = 0;

    return {bestLabel, scores};
}

//void predictData(const MnbModel & model, const string & pathname) {
void predictData(const MnbModel &, const string &) {

    // TODO

}

//////////////////////////////////////////////////////////////////////
// GUI
//////////////////////////////////////////////////////////////////////

struct GuiData {
    const string _windowName;
    int _width;
    int _height;
    cv::Mat _img;
};

void mouseCallback(int event, int x, int y, int flags, void * ptrData) {

    assert(ptrData);
    GuiData & guiData = *static_cast<GuiData*>(ptrData);

    if (event == cv::EVENT_MOUSEMOVE and flags == cv::EVENT_FLAG_LBUTTON) {
        cv::circle(guiData._img, cv::Point{x, y}, 2, cv::Scalar::all(0.0), 2);
        cv::imshow(guiData._windowName, guiData._img);
    }

    if (event == cv::EVENT_MOUSEMOVE and flags == cv::EVENT_FLAG_RBUTTON) {
        cv::circle(guiData._img, cv::Point{x, y}, 4, cv::Scalar::all(255.0), 4);
        cv::imshow(guiData._windowName, guiData._img);
    }
}

//////////////////////////////////////////////////////////////////////
// main
//////////////////////////////////////////////////////////////////////

int main() {

    // train model
    int width = 64;
    int height = 64;
    MnbModel model = initAndTrainModel("data/nist64/training", width*height);

    // test using some data
    predictData(model, "data/nist64/test");

    // test using interactive gui
    cv::Mat emptyImg = 255 * cv::Mat::ones(height, width, CV_8UC1);
    GuiData guiData {"Digit predictor", width, height, emptyImg.clone()};
    cv::namedWindow(guiData._windowName); 
    cv::setMouseCallback(guiData._windowName, mouseCallback, &guiData);

    cout << "\nDraw digit in window: \n"
        << "- left click -> draw\n"
        << "- right click -> erase\n"
        << "- space -> clear image\n"
        << "- enter -> predict digit\n"
        << "- esc -> quit\n" << endl;

    while (true) {
        // refresh
        cv::imshow(guiData._windowName, guiData._img);
        cv::resizeWindow(guiData._windowName, guiData._width, guiData._height);

        // handle keyboard
        int key = cv::waitKey() % 0x100;
        if (key == 27)   // esc
            break;
        if (key == 32)   // space
            guiData._img = emptyImg.clone();
        if (key == 10) { // enter
            auto p = predictImage(model, guiData._img);
            int kBest = p.first;
            cv::Mat scores = p.second;
            cout << "predict label " << model._labels[kBest] 
                << " with score " << scores.at<double>(kBest) << endl;
        }
    }

    return 0;
}

