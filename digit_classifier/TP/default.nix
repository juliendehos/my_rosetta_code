# { pkgs ? (import <nixpkgs> {}) }:
# 
# with pkgs;
# 
# let 
# 
#   _pyPkgs = python3Packages;
# 
#   _pyopencv3 = _pyPkgs.opencv3.override {
# 
#     enableTIFF = false;
#     enableWebP = false;
#     enableEXR = false;
#     enableJPEG2K = false;
#     enableContrib = false;
# 
#     enableGtk3 = true;
#     enableFfmpeg = true;
#   };
# 
# in 
# 
# _pyPkgs.buildPythonPackage {
#   name = "digit_gui";
#   src = ./.;
#   buildInputs = [ 
#     gnumake
#     boost
#     _pyPkgs.matplotlib
#     _pyPkgs.numpy
#     _pyPkgs.tkinter
#     _pyopencv3
#   ];
# }
# 

#{ pkgs ? import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/17.09.tar.gz") {} }:

{ pkgs ? import <nixpkgs> {} }:

with pkgs;

let

  _opencv3 = opencv3.override {

    enableTIFF = false;
    enableWebP = false;
    enableEXR = false;
    enableJPEG2K = false;
    enableContrib = false;

    enableGtk3 = true;
    enableFfmpeg = true;
  };

in

stdenv.mkDerivation {

  name = "digit_classifier";

  src = ./.;

  buildInputs = [
    boost
    pkgconfig
    _opencv3
  ];

  enableParallelBuilding = true;

  installPhase = ''
    mkdir -p $out/bin
    mv *.out $out/bin
  '';

}


