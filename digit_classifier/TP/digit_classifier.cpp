#include <boost/filesystem.hpp>
#include <boost/range/iterator_range.hpp>
#include <opencv2/opencv.hpp>

using namespace std;
namespace fs = boost::filesystem;

struct MnbModel {
    vector<string> _labels;
    unsigned _nbClasses;
    unsigned _nbPixels;
    cv::Mat _logPriors;
    cv::Mat _logLikelyhoods;
};

int main() {

    const string TRAINING_DIR = "data/nist64/training";
    const string TEST_DIR = "data/nist64/test";
    const unsigned NB_PIXELS = 64*64;

    // init model
    MnbModel model;
    for(const auto & labelEntry : boost::make_iterator_range(fs::directory_iterator(TRAINING_DIR), {})) {
        model._labels.emplace_back(labelEntry.path().stem().string());
    }
    model._nbClasses = model._labels.size();
    model._nbPixels = NB_PIXELS;
    model._logPriors = cv::Mat::zeros(model._nbClasses, 1, CV_64F);
    model._logLikelyhoods = cv::Mat::zeros(model._nbClasses, model._nbPixels, CV_64F);

    // train model
    cout << "\nlabel nbImages maxLogLikelyhood" << endl;
    for (unsigned k=0; k<model._nbClasses; k++) {
        const string & label = model._labels[k];
        cv::Mat sum_img = cv::Mat::zeros(1, model._nbPixels, CV_64F);
        for(const auto & imgEntry : boost::make_iterator_range(fs::directory_iterator(TRAINING_DIR + "/" + label), {})) { 
            string filename = imgEntry.path().string();
            cv::Mat image;
            cv::imread(filename, cv::IMREAD_GRAYSCALE).reshape(1, 1).convertTo(image, CV_64F);
            sum_img += cv::Mat::ones(1, model._nbPixels, CV_64F) - (image / 255.0);
            model._logPriors.at<double>(k)++;
        }
        cv::Mat logLikelyhood = sum_img + cv::Mat::ones(1, model._nbPixels, CV_64F);
        logLikelyhood /= cv::sum(logLikelyhood)[0];
        cv::log(logLikelyhood, logLikelyhood);
        logLikelyhood.copyTo(model._logLikelyhoods.row(k));
        double max_val;
        cv::minMaxLoc(logLikelyhood, nullptr, &max_val, nullptr, nullptr);
        cout << model._labels[k] << " " << model._logPriors.at<double>(k) << " " << max_val << endl;
    }
    cv::log(model._logPriors/cv::sum(model._logPriors)[0], model._logPriors);

    // test
    unsigned hsTests = 0;
    unsigned nbTests = 0;
    cout << "\nfilename truth result" << endl;
    for(const auto & imgEntry : boost::make_iterator_range(fs::directory_iterator(TEST_DIR), {})) { 
        string filename = imgEntry.path().string();
        cv::Mat image;
        cv::imread(filename, cv::IMREAD_GRAYSCALE).reshape(1, model._nbPixels).convertTo(image, CV_64F);
        image = cv::Mat::ones(model._nbPixels, 1, CV_64F) - (image / 255.0);
        cv::Mat scores = model._logPriors + model._logLikelyhoods * image;
        cv::Point best_p;
        cv::minMaxLoc(scores, nullptr, nullptr, nullptr, &best_p);
        char result = model._labels[best_p.y][0];
        string stem = imgEntry.path().stem().string();
        char truth = stem[7];
        cout << stem << " " << truth << " " << result << endl;
        if (truth != result) 
            hsTests++;
        nbTests++;
    }
    cout << "\nerror rate: " << double(hsTests) / double(nbTests) << endl;

    return 0;
}

