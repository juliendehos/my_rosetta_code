#!/usr/bin/octave -qf

max_imgs = 2000;
training_dir = 'data/nist64/training';
test_dir = 'data/nist64/test';
nbPixels = 64*64;

% model
labels = dir(training_dir)(3:end);  % remove "." and ".."
nbClasses = length(labels);
logPriors = zeros(nbClasses);
logLikelyhoods = zeros(nbClasses, nbPixels);

% training
printf('label nb_imgs_k max_logLikelyhood\n');
for k = 1:length(labels)
    label = labels(k).name;
    dir_path = strcat(training_dir, '/', label, '/');
    sum_img = zeros(1, nbPixels);
    for f = dir(dir_path)(3:end)'
        file = strcat(dir_path, f.name);
        img = 1 - reshape(imread(file), 1, nbPixels);
        sum_img += img;
        logPriors(k) += 1;
        if logPriors(k) >= max_imgs
            break
        end
    end
    sum_img += 1;
    logLikelyhoods(k,:) = log(sum_img / sum(sum_img));
    printf('%s %f %f\n', label, logPriors(k), max(logLikelyhoods(k,:)));
end
logPriors = log(logPriors / sum(logPriors));

% test
nb_tests = 0;
hs_tests = 0;
printf('\nfilename truth result\n');
for f = dir(test_dir)(3:end)'
    file = strcat(test_dir, '/', f.name);
    img = 1 - reshape(imread(file), nbPixels, 1);
    scores = logPriors + logLikelyhoods * img;
    [_, best_k] = max(scores);
    result = labels(best_k).name;
    truth = (f.name)(8);
    printf('%s %s %s\n', file, truth, result);
    nb_tests += 1;
    hs_tests += truth != result;
end
printf('\nerror rate: %f\n', hs_tests/nb_tests);

