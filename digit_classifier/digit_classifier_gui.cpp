#include <boost/filesystem.hpp>
#include <boost/range/iterator_range.hpp>
#include <opencv2/opencv.hpp>

using namespace std;
namespace fs = boost::filesystem;

//////////////////////////////////////////////////////////////////////
// multimodal naive bayesian model
//////////////////////////////////////////////////////////////////////

struct MnbModel {

    vector<string> _labels;
    unsigned _nbClasses;
    unsigned _nbPixels;
    cv::Mat _logPriors;
    cv::Mat _logLikelyhoods;

    MnbModel(const string & trainingDir, unsigned nbPixels) {
        for(const auto & labelEntry : boost::make_iterator_range(fs::directory_iterator(trainingDir), {}))
            _labels.emplace_back(labelEntry.path().stem().string());

        _nbClasses = _labels.size();
        _nbPixels = nbPixels;
        _logPriors = cv::Mat::zeros(_nbClasses, 1, CV_32F);
        _logLikelyhoods = cv::Mat::zeros(_nbClasses, _nbPixels, CV_32F);
    }
};

MnbModel trainModel(const string & pathname, unsigned nbPixels) {

    MnbModel model(pathname, nbPixels);

    cout << endl << "label nbImagesK maxLogLikelyhood" << endl;
    for (unsigned k=0; k<model._nbClasses; k++) {
        const string & label = model._labels[k];
        cv::Mat sumImage = cv::Mat::zeros(1, model._nbPixels, CV_32F);
        for(const auto & imageEntry : boost::make_iterator_range(fs::directory_iterator(pathname + "/" + label), {})) { 
            string filename = imageEntry.path().string();
            cv::Mat image;
            cv::imread(filename, cv::IMREAD_GRAYSCALE).reshape(1, 1).convertTo(image, CV_32F);
            cv::threshold(image, image, 127.0, 1.0, cv::THRESH_BINARY_INV);
            sumImage += image;
            model._logPriors.at<float>(k)++;
        }
        cv::Mat logLikelyhood = sumImage + cv::Mat::ones(1, nbPixels, CV_32F);
        logLikelyhood /= cv::sum(logLikelyhood)[0];
        cv::log(logLikelyhood, logLikelyhood);
        logLikelyhood.copyTo(model._logLikelyhoods.row(k));
        double maxVal;
        cv::minMaxLoc(logLikelyhood, nullptr, &maxVal, nullptr, nullptr);
        cout << model._labels[k] << " " << model._logPriors.at<float>(k) << " " << maxVal << endl;
    }
    cv::log(model._logPriors/cv::sum(model._logPriors)[0], model._logPriors);

    return model;
}

pair<int,cv::Mat> predictImage(const MnbModel & model, const cv::Mat & image) {

    // format image
    cv::Mat X;
    image.convertTo(X, CV_32F);
    X = X.reshape(1, model._nbPixels);
    cv::threshold(X, X, 127.0, 1.0, cv::THRESH_BINARY_INV);
    assert(X.rows == int(model._nbPixels));
    assert(X.cols == 1);

    // find best match
    cv::Mat scores = model._logPriors + model._logLikelyhoods * X;
    cv::Point pBest;
    cv::minMaxLoc(scores, nullptr, nullptr, nullptr, &pBest);
    return {pBest.y, scores};
}

void predictData(const MnbModel & model, const string & pathname) {

    // traverse images
    fs::path path(pathname);
    unsigned nbTestHs = 0;
    unsigned nbTestImg = 0;
    cout << "\nfilename truth result score" << endl;
    for(const auto & imageEntry : boost::make_iterator_range(fs::directory_iterator(path), {})) { 

        // open image 
        string filename = imageEntry.path().string();
        cv::Mat image = cv::imread(filename, cv::IMREAD_GRAYSCALE);

        // find best label
        auto p = predictImage(model, image);
        int kBest = p.first;
        cv::Mat scores = p.second;

        // validation
        string stem = imageEntry.path().stem().string();
        char truth = stem[7];
        char result = model._labels[kBest][0];
        cout << stem << " " << truth << " " << result << " " << scores.at<float>(kBest) << endl;
        if (truth != result) 
            nbTestHs++;
        nbTestImg++;
    }

    cout << "\nerror rate: " << float(nbTestHs) / float(nbTestImg) << endl;
}

//////////////////////////////////////////////////////////////////////
// GUI
//////////////////////////////////////////////////////////////////////

struct GuiData {
    const string _windowName;
    int _width;
    int _height;
    cv::Mat _img;
};

void mouseCallback(int event, int x, int y, int flags, void * ptrData) {

    assert(ptrData);
    GuiData & guiData = *static_cast<GuiData*>(ptrData);

    if (event == cv::EVENT_MOUSEMOVE and flags == cv::EVENT_FLAG_LBUTTON) {
        cv::circle(guiData._img, cv::Point{x, y}, 2, cv::Scalar::all(0.0), 2);
        cv::imshow(guiData._windowName, guiData._img);
    }

    if (event == cv::EVENT_MOUSEMOVE and flags == cv::EVENT_FLAG_RBUTTON) {
        cv::circle(guiData._img, cv::Point{x, y}, 4, cv::Scalar::all(255.0), 4);
        cv::imshow(guiData._windowName, guiData._img);
    }
}

//////////////////////////////////////////////////////////////////////
// main
//////////////////////////////////////////////////////////////////////

int main() {

    // train model
    int width = 64;
    int height = 64;
    MnbModel model = trainModel("data/nist64/training", width*height);

    // test using some data
    predictData(model, "data/nist64/test");

    // test using interactive gui
    cv::Mat emptyImg = 255 * cv::Mat::ones(height, width, CV_8UC1);
    GuiData guiData {"Digit predictor", width, height, emptyImg.clone()};
    cv::namedWindow(guiData._windowName); 
    cv::setMouseCallback(guiData._windowName, mouseCallback, &guiData);

    cout << "\nDraw digit in window: \n"
        << "- left click -> draw\n"
        << "- right click -> erase\n"
        << "- space -> clear image\n"
        << "- enter -> predict digit\n"
        << "- esc -> quit\n" << endl;

    while (true) {
        // refresh
        cv::imshow(guiData._windowName, guiData._img);
        cv::resizeWindow(guiData._windowName, guiData._width, guiData._height);

        // handle keyboard
        int key = cv::waitKey() % 0x100;
        if (key == 27)   // esc
            break;
        if (key == 32)   // space
            guiData._img = emptyImg.clone();
        if (key == 10) { // enter
            auto p = predictImage(model, guiData._img);
            int kBest = p.first;
            cv::Mat scores = p.second;
            cout << "predict label " << model._labels[kBest] 
                << " with score " << scores.at<float>(kBest) << endl;
        }
    }

    return 0;
}

