#!/usr/bin/env python2

import os
import numpy
import skimage 
from skimage import io
from sklearn.naive_bayes import MultinomialNB

max_imgs = 2000
max_imgs_total = max_imgs*10
training_dir = 'data/nist64/training'
test_dir = 'data/nist64/test'
nbPixels = 64*64

# training
nb_imgs = 0
X = numpy.empty((max_imgs_total,nbPixels), numpy.uint8)
y = numpy.empty((max_imgs_total), int)
print 'label nb_imgs_k'
for label in os.listdir(training_dir):
    nb_imgs_k = 0
    label_path = os.path.join(training_dir, label)
    for img_file in os.listdir(label_path):
        img_path = os.path.join(label_path, img_file)
        img = io.imread(img_path, as_grey=True).reshape(1,nbPixels)
        X[nb_imgs,:] = (numpy.ones(nbPixels, numpy.uint8)*255) - img
        y[nb_imgs] = label
        nb_imgs += 1
        nb_imgs_k += 1
        if nb_imgs_k >= max_imgs:
            break

    print label, nb_imgs_k

X = X[:nb_imgs,:]
y = y[:nb_imgs]
clf = MultinomialNB()
clf.fit(X, y)

# test
nb_tests = 0
hs_tests = 0
print '\nfilename truth result'
for test_file in os.listdir(test_dir):
    test_path = os.path.join(test_dir, test_file)
    img = io.imread(test_path, as_grey=True).reshape(1,nbPixels)
    test_img = (numpy.ones(nbPixels, numpy.uint8)*255) - img
    truth = int(test_file[7])
    result = clf.predict(test_img)[0]
    nb_tests += 1
    if (truth != result):
        hs_tests += 1
    print test_file, truth, result

print '\nerror rate:', float(hs_tests)/float(nb_tests)

