using CVCore, CVImgCodecs

max_imgs = 2000
training_dir = "data/nist64/training"
test_dir = "data/nist64/test"
nb_pixels = 64*64

# model
labels = readdir(training_dir)
nb_classes = length(labels)
log_priors = zeros(nb_classes)
log_likelyhoods = zeros(nb_classes, nb_pixels)

# training
println("label nb_imgs_k max_logLikelyhood")
for (k,label) in enumerate(labels)
    dir_path = training_dir * "/" * label * "/"
    sum_img = zeros(1, nb_pixels)
    for filename in readdir(dir_path)
        cv_img = imread(dir_path * filename)
        jl_img = convert(Array{Float64,2}, cv_img[:,:,1]/255)
        sum_img += 1.0 - reshape(jl_img, 1, nb_pixels)
        log_priors[k] += 1.0
        if log_priors[k] >= max_imgs
            break
        end
    end
    sum_img += 1
    log_likelyhoods[k,:] = log(sum_img / sum(sum_img))
    println(label, " ", log_priors[k], " ", max(log_likelyhoods[k]))
end
log_priors = log(log_priors / sum(log_priors))

# test
nb_tests = 0
hs_tests = 0
println("\nfilename truth result")
for test_file in readdir(test_dir)
    cv_img = imread(test_dir * "/" * test_file)
    jl_img = convert(Array{Float64,2}, cv_img[:,:,1]/255)
    test_img = 1.0 - reshape(jl_img, nb_pixels, 1)
    scores = log_priors + (log_likelyhoods * test_img)
    best_k = indmax(scores)
    truth = string(test_file[8])
    result = labels[best_k]
    println(test_file, " ", truth, " ", result)
    nb_tests += 1
    if truth != result
        hs_tests += 1
    end
end
println("\nerror rate: ", hs_tests / nb_tests)

