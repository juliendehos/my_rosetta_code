const NB_GAMES = 1000

using StaticArrays
const EYE9 = eye(SMatrix{9, 9, Int})
const BOARD_LUT = SVector(EYE9, 2*EYE9)

struct Tictactoe
  board::SVector{9,Int}
  nbFreeMoves::Int
  currentPlayer::Int
  nextPlayer::Int
end

@inline Tictactoe() = Tictactoe( [0, 0, 0, 0, 0, 0, 0, 0, 0] , 9, 1, 2)

@inline function computeFinish(b::SVector{9,Int}, nbFreeMoves::Int)
  if b[1] != 0 && b[1] == b[2] == b[3]
    true, b[1]
  elseif b[4] != 0 && b[4] == b[5] == b[6]
    true, b[4]
  elseif b[7] != 0 && b[7] == b[8] == b[9]
    true, b[7]
  elseif b[1] != 0 && b[1] == b[4] == b[7]
    true, b[1]
  elseif b[2] != 0 && b[2] == b[5] == b[8]
    true, b[2]
  elseif b[3] != 0 && b[3] == b[6] == b[9]
    true, b[3]
  elseif b[1] != 0 && b[1] == b[5] == b[9]
    true, b[1]
  elseif b[3] != 0 && b[3] == b[5] == b[7]
    true, b[3]
  elseif nbFreeMoves == 0  # no winner, no empty cell -> draw
    true, 0
  else  # no winner, empty cell -> game not finished
    false, 0
  end
end

@inline function findIndex(board::SVector{9,Int}, moveIndex::Int)
  nbFree = 0
  k = 1
  while k<=9 
    if board[k] == 0
      nbFree += 1
      if nbFree == moveIndex
        break
      end
    end
    k += 1
  end
  k
end

@inline function playIndex(game::Tictactoe, moveIndex::Int)
  k = findIndex(game.board, moveIndex)
  if k <= 9
    board = game.board + BOARD_LUT[game.currentPlayer][k,:]
    Tictactoe(board, game.nbFreeMoves-1, game.nextPlayer, game.currentPlayer)
  else
    game
  end
end

abstract type Bot end

@inline function runGame(game::Tictactoe, bot1::Bot, bot2::Bot)
  while true
    finished, winner = computeFinish(game.board, game.nbFreeMoves)
    if finished
      return winner
    end
    moveIndex = game.currentPlayer == 1 ? genmove(game, bot1) : genmove(game, bot2)
    game = playIndex(game, moveIndex)
  end
end

struct RandomBot <: Bot end

@inline genmove(game::Tictactoe, bot::RandomBot) = trunc(Int,1+rand()*game.nbFreeMoves)
#genmove(game::Tictactoe, bot::RandomBot) = ceil(Int,rand()*game.nbFreeMoves)
#genmove(game::Tictactoe, bot::RandomBot) = rand(1:game.nbFreeMoves)

struct McBot <: Bot
  maxSims::Int
end

@inline function evalMove(game::Tictactoe, moveIndex::Int, nbSims::Int)
  bot = RandomBot()
  player = game.currentPlayer
  game1 = playIndex(game, moveIndex)
  evalMove1(unused) = runGame(game1, bot, bot) == player ? 1 : 0
  mapreduce(evalMove1, (+), 0, 1:nbSims)
end

@inline function genmove(game::Tictactoe, bot::McBot)
  nbMoves = game.nbFreeMoves
  nbMoveSims = max(1, div(bot.maxSims, nbMoves))
  simulate(moveIndex) = evalMove(game, moveIndex, nbMoveSims)
  wins = map(simulate, [moveIndex for moveIndex in 1:nbMoves])
  indmax(wins)
end

@inline function runGames(game::Tictactoe, bot1::Bot, bot2::Bot, nbGames::Int)
  res = [0, 0, 0]
  for _ in 1:nbGames
    winner = 1 + runGame(game, bot1, bot2)
    res[winner] += 1
  end
  res
end

function runXp(nbSims::Int)
  game = Tictactoe()
  botA = RandomBot()
  botB = McBot(nbSims)
  resAB = runGames(game, botA, botB, NB_GAMES)
  resBA = runGames(game, botB, botA, NB_GAMES)
  println("RandomBot McBot ", nbSims, " ", NB_GAMES, " ", 
          resAB[1], " ", resAB[2], " ", resAB[3], " ", 
          resBA[1], " ", resBA[2], " ", resBA[3], " ", 
          resAB[1]+resBA[1], " ", resAB[2]+resBA[3], " ", resAB[3]+resBA[2])
end

println("botA botB nbSims nbGames drawsA1B2 winsA1 winsB2 drawsB1A2 winsB1 winsA2 draws winsA winsB")
vecNbSims = [8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096]
map(runXp, vecNbSims)

