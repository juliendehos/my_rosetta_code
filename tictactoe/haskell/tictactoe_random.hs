import Data.Vector as V (filter, fromList, replicate, update, Vector, zip, (!))
import System.Random.Mersenne.Pure64 (randomDouble, newPureMT, PureMT)
import Text.Printf (printf)

gNbGames :: Int
gNbGames = 1000

data Tictactoe = Tictactoe { 
  board :: Vector Int,
  nbFreeMoves :: Int,
  currentPlayer :: Int,
  nextPlayer :: Int
} deriving Show

newTictactoe :: Tictactoe
newTictactoe = Tictactoe (V.replicate 9 0) 9 1 2

boardIndices :: Vector Int
boardIndices = fromList [0..8]

computeFinish :: Vector Int -> Int -> (Bool, Int)
computeFinish b nbFreeMoves 
  | (b!0) /= 0 && (b!0) == (b!1) && (b!1) == (b!2) = (True, (b!0))
  | (b!3) /= 0 && (b!3) == (b!4) && (b!4) == (b!5) = (True, (b!3))
  | (b!6) /= 0 && (b!6) == (b!7) && (b!7) == (b!8) = (True, (b!6))
  | (b!0) /= 0 && (b!0) == (b!3) && (b!3) == (b!6) = (True, (b!0))
  | (b!1) /= 0 && (b!1) == (b!4) && (b!4) == (b!7) = (True, (b!1))
  | (b!2) /= 0 && (b!2) == (b!5) && (b!5) == (b!8) = (True, (b!2))
  | (b!0) /= 0 && (b!0) == (b!4) && (b!4) == (b!8) = (True, (b!0))
  | (b!2) /= 0 && (b!2) == (b!4) && (b!4) == (b!6) = (True, (b!2))
  | nbFreeMoves == 0 = (True, 0)  -- no winner, no empty cell -> draw
  | otherwise = (False, 0)  -- no winner, empty cell -> game not finished

findIndex :: Vector Int -> Int -> Int
findIndex board moveIndex = fst $ freeCells!moveIndex
  where boardZip = V.zip boardIndices board
        freeCells = V.filter ((==0) . snd) boardZip

playIndex :: Tictactoe -> Int -> Tictactoe
playIndex game moveIndex = Tictactoe board' nbFreeMoves' currentPlayer' nextPlayer'
  where b = board game
        boardIndex = findIndex b moveIndex
        board' = update b (fromList [(boardIndex, (currentPlayer game))])
        nbFreeMoves' = (nbFreeMoves game) - 1
        currentPlayer' = nextPlayer game
        nextPlayer' = currentPlayer game

type Bot = Tictactoe -> PureMT -> (Int, PureMT) 

myrand n r0 = (v, r1)
    where (v1, r1) = randomDouble r0
          nd = fromIntegral n
          v = floor $ v1*nd

randomBot :: Bot
randomBot game rng = 
  let nbMoves = nbFreeMoves game
      (m, r) = myrand nbMoves rng
  in if m>=0 && m<nbMoves then (m, r) else error $ "random " ++ (show m) ++ " " ++ (show nbMoves)

mcBot :: Int -> Bot
mcBot maxSims game = simulate 0 0 0
  where nbMoves = nbFreeMoves game
        nbMoveSims = max 1 $ div maxSims nbMoves
        simulate indexMove bestIndex bestWins myRng = 
          if indexMove == nbMoves
          then (bestIndex, myRng)
          else simulate (indexMove + 1) bestIndex' bestWins' myRng'
              where (myNbWins, myRng') = evalMove game indexMove nbMoveSims myRng
                    (bestIndex', bestWins') = if bestWins < myNbWins 
                                              then (indexMove, myNbWins)
                                              else (bestIndex, bestWins)

evalMove :: Tictactoe -> Int -> Int -> PureMT -> (Int, PureMT)
evalMove game moveIndex nbSims rng = if (nbFreeMoves game) == 1 then (0, rng) else
  let game1 = playIndex game moveIndex
      player = currentPlayer game
      evalMove1 0 myNbWins myRng = (myNbWins, myRng)
      evalMove1 myNbSims myNbWins myRng = evalMove1 (myNbSims-1) myNbWins' myRng'
        where (winner, myRng') = runGame game1 randomBot randomBot myRng
              myNbWins' = if winner == player then myNbWins+1 else myNbWins
  in evalMove1 nbSims 0 rng

runGame :: Tictactoe -> Bot -> Bot -> PureMT -> (Int, PureMT)
runGame game bot1 bot2 rng = if finished then (winner, rng') else continue
  where bot = if currentPlayer game == 1 then bot1 else bot2
        (moveIndex, rng') = bot game rng
        game' = playIndex game moveIndex
        (finished, winner) = computeFinish (board game') (nbFreeMoves game')
        continue = runGame game' bot1 bot2 rng'

runGames :: Tictactoe -> Bot -> Bot -> Int -> PureMT -> (Int, Int, Int, PureMT)
runGames game bot1 bot2 nbGames rng = runGamesAux game bot1 bot2 nbGames (0, 0, 0, rng)
  where runGamesAux game bot1 bot2 nbGames res@(draws, wins1, wins2, rng) = 
          if nbGames == 0
          then res
          else let (winner, rng') = runGame game bot1 bot2 rng
                   res' = case winner of 0 -> (draws+1, wins1, wins2, rng')
                                         1 -> (draws, wins1+1, wins2, rng')
                                         2 -> (draws, wins1, wins2+1, rng')
                                         _ -> error $ "unknown winner: " ++ (show winner)
               in runGamesAux game bot1 bot2 (nbGames-1) res'

runXp :: Int -> PureMT -> IO PureMT
runXp maxSims rng = do
  printf "RandomBot McBot %d %d %d %d %d %d %d %d %d %d %d\n" 
    maxSims gNbGames drawsA1B2 winsA1 winsB2 drawsB1A2 winsB1 winsA2 draws winsA winsB
  return rng''
    where (drawsA1B2, winsA1, winsB2, rng') = runGames newTictactoe randomBot (mcBot maxSims) gNbGames rng
          (drawsB1A2, winsB1, winsA2, rng'') = runGames newTictactoe (mcBot maxSims) randomBot gNbGames rng'
          draws = drawsA1B2 + drawsB1A2
          winsA = winsA1 + winsA2
          winsB = winsB1 + winsB2

main = do
  putStrLn "botA botB nbSims nbGames drawsA1B2 winsA1 winsB2 drawsB1A2 winsB1 winsA2 draws winsA winsB"
  -- newPureMT >>= runXp 10 >>= runXp 100 >>= runXp 1000 >>= runXp 10000
  newPureMT >>= runXp 10 >>= runXp 100 >>= runXp 1000 
