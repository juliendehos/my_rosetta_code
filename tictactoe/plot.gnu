set terminal png size 640,480
set style data linespoints
set grid xtics ytics
set logscale x
set xlabel 'nbSims'
set ylabel 'nbWins'
set key left top

set out 'out.png'
set title 'tictactoe'
plot 'out.dat' using 3:13 lw 2 title 'Monte-Carlo', 'out.dat' using 3:12 lw 2 title "Random", 'out.dat' using 3:11 lw 2 title 'Draws'

