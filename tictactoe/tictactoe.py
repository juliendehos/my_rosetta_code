#!/usr/bin/env python3

import copy
import random

NB_GAMES = 1000

class Tictactoe:
    def __init__(self):
        self.board = 9*[0]
        self.nbFreeMoves = 9
        self.currentPlayer = 1
        self.nextPlayer = 2
        self.finished = False
        self.winner = 0

    def playIndex(self, moveIndex):
        # find index
        nbFree = moveIndex
        k = 0
        while k < 9:
            if self.board[k] == 0:
                nbFree -= 1
            if nbFree <= 0:
                break
            k += 1
        # update board
        if k == 9:
            return False
        self.nbFreeMoves -= 1
        self.board[k] = self.currentPlayer;
        self.currentPlayer, self.nextPlayer = self.nextPlayer, self.currentPlayer
        # check winner
        b = self.board
        self.finished = True
        self.winner = 0
        if b[0] != 0 and b[0] == b[1] == b[2]:
            self.winner = b[0]
        elif b[3] != 0 and b[3] == b[4] == b[5]:
            self.winner = b[3]
        elif b[6] != 0 and b[6] == b[7] == b[8]:
            self.winner = b[6]
        elif b[0] != 0 and b[0] == b[3] == b[6]:
            self.winner = b[0]
        elif b[1] != 0 and b[1] == b[4] == b[7]:
            self.winner = b[1]
        elif b[2] != 0 and b[2] == b[5] == b[8]:
            self.winner = b[2]
        elif b[0] != 0 and b[0] == b[4] == b[8]:
            self.winner = b[0]
        elif b[2] != 0 and b[2] == b[4] == b[6]:
            self.winner = b[2]
        elif self.nbFreeMoves != 0:
            self.finished = False
        return True

class Bot:
    pass

class RandomBot(Bot):
    def genmove(self, game):
        return random.randint(0, game.nbFreeMoves-1)

def runGame(game, bot1, bot2):
    while not game.finished:
        if game.currentPlayer == 1:
            moveIndex = bot1.genmove(game)
        else:
            moveIndex = bot2.genmove(game)
        playOk = game.playIndex(moveIndex)
        if not playOk:
            print("runGame -> invalid play")
            quit()

class McBot(Bot):
    def __init__(self, maxSims):
        self.maxSims = maxSims
        self.randomBot = RandomBot()

    def genmove(self, game):
        player = game.currentPlayer
        nbMoves = game.nbFreeMoves
        nbMoveSims = max(1, self.maxSims // nbMoves)
        bestIndex = 0
        bestWins = 0
        for moveIndex in range(nbMoves):
            moveWins = 0
            game1 = copy.deepcopy(game)
            game1.playIndex(moveIndex)
            for _ in range(nbMoveSims):
                gameN = copy.deepcopy(game1)
                runGame(gameN, self.randomBot, self.randomBot)
                if gameN.winner == player:
                    moveWins += 1
            if moveWins > bestWins:
                bestWins = moveWins
                bestIndex = moveIndex
        return bestIndex

def runGames(game, bot1, bot2, nbGames):
    res = [0, 0, 0]
    for _ in range (nbGames):
        testGame = copy.deepcopy(game)
        runGame(testGame, bot1, bot2)
        winner = testGame.winner
        res[winner] += 1
    return res

def runXp(nbSims):
    game = Tictactoe()
    botA = RandomBot()
    botB = McBot(nbSims)
    drawsA1B2, winsA1, winsB2 = runGames(game, botA, botB, NB_GAMES)
    drawsB1A2, winsB1, winsA2 = runGames(game, botB, botA, NB_GAMES)
    print("RandomBot McBot", nbSims, NB_GAMES, drawsA1B2, winsA1, winsB2, 
            drawsB1A2, winsB1, winsA2, drawsA1B2+drawsB1A2, winsA1+winsA2, winsB1+winsB2) 

def main():
    print("botA botB nbSims nbGames drawsA1B2 winsA1 winsB2 drawsB1A2 winsB1 winsA2 draws winsA winsB")
    listNbSims = [8, 16, 32, 64, 128]
    #listNbSims = [8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096]
    for nbSims in listNbSims:
        runXp(nbSims)

if __name__ == "__main__":
    main()

