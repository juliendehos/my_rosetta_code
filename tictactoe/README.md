# tictactoe

The tictactoe game with two bots (random and Monte-Carlo).

| file                  | time |
|-----------------------|------|
| tictactoe.cpp         | 10s  |
| tictactoe.jl          | 11s  |


## run benchmarks

```
make
time ./tictactoe_cpp.out

julia tictactoe.jl
time julia tictactoe.jl

stack build 
stack exec tictactoe_random

```

## draw plot

```
./tictactoe_cpp.out > out.dat
gnuplot plot.gnu
xdg-open out.png
```

![](doc_plot.png)

## todo

- python
- haskell

